import { Component, ViewChild } from '@angular/core';
import { NavParams, NavController, ToastController } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { HomePage } from '../home/home';
import { PatientService } from '../../providers/patient-service';
import * as moment from 'moment';
import * as _ from 'underscore';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-add-patient',
  templateUrl: 'add-patient.html'
})

export class AddPatientPage {
  username: string;
  patient: any = {
    sex: 'M',
    dateOfBirth: '',
    dateOfAdmission: moment().format()
  };
  showBackButton: boolean = false;
  uid: any;
  mode: String;
  maxDate: any = moment().format('');
  currentHospitalId: string;
  moment = moment;
  @ViewChild('patientForm') form: any;

  constructor(
    public barcodeScanner: BarcodeScanner,
    public helper: Helper,
    public navCtrl: NavController,
    public params: NavParams,
    public patientService: PatientService,
    public toastCtrl: ToastController
  ) {
    this.showBackButton = this.params.get("showBackButton");
    this.currentHospitalId = this.params.get("hospitalId");
    this.mode = this.params.get("mode");
    if (this.params.get("patient")) {
      this.patient = _.clone(this.params.get("patient"));
      this.patient.dateOfAdmission = moment(this.patient.dateOfAdmission, 'MM/DD/YYYY').format('');
      this.patient.dateOfBirth = moment(this.patient.dateOfBirth, 'MM/DD/YYYY').format('');
    }
  }

  getMinAdmissionDate() {
    return this.patient.dateOfBirth || moment().subtract(50, 'years').format('');
  }

  async savePatient() {
    try {
      this.helper.showLoading();
      this.patient['hospitalId'] = this.patient.hospitalId ? this.patient.hospitalId : this.currentHospitalId;

      Object.keys(this.patient).forEach((eachKey) => {
        if (eachKey == 'room') {
          this.patient[eachKey] = this.patient[eachKey] || null;
        }
        else {
          !this.patient[eachKey] ? delete this.patient[eachKey] : '';
        }
      })
      let patient = _.clone(this.patient);
      patient.dateOfAdmission = moment(patient.dateOfAdmission).format('MM/DD/YYYY');
      patient.dateOfBirth = moment(patient.dateOfBirth).format('MM/DD/YYYY');
      let res = await this.patientService.upsertPatient(patient);
      let patientId = _.values(res)[0].patientId, action = this.mode == 'EDIT' ? "UPDATE" : "CREATE";
      let patientEntitiesObj = {
        patientId: patientId,
        hospitalId: this.currentHospitalId,
        createdOn: moment().format('YYYYMMDD'),
        updatedOn: moment().format('YYYYMMDD'),
        billDetails: {
          diagnosis: [],
          em_code: [],
          procedures: []
        }
      }
      console.log(this.mode);
      if (this.mode != 'EDIT') {
        console.log("Adding damn entities")
        await this.patientService.upsertPatientEntities(patientEntitiesObj, action);
      }
      this.helper.hideLoading();
      this.helper.showMessage("Patient saved successfully");
      this.navCtrl.setRoot(HomePage);
    } catch (err) {
      console.log(err);
      this.helper.hideLoading();
      this.helper.showMessage("Error occured while saving patient");
    }
  }

  launchBarcodeScanner() {
    let self: any = this;
    this.barcodeScanner.scan()
      .then((barcodeData: any) => {
        self.patient.medicalRecordNumber = barcodeData.text || '';
      })
      .catch((err) => {
        console.log('Failed to lauch barcode scanner');
      })
  }
}
