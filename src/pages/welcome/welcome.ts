import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Events, MenuController, NavController, Slides } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HospitalCard } from '../../components/hospital-card/hospital-card';
import { Helper } from '../../providers/helper';
import * as _ from 'underscore';
import { SubscriptionListing } from '../../subscriptions-module';
import { Auth } from 'aws-amplify';
import { LoginComponent } from '../authentication/login/login';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})

export class WelcomePage implements OnDestroy {
  previousSlide: string;
  buttonText: string = 'Next';
  uid: any;
  valid: any = {
    security: false,
    hospitals: 0,
    specialization: false,
    subscription: true
  }
  userAppReadySubscription: any;
  nickname: any;
  @ViewChild(Slides) slides: Slides;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public helper: Helper,
    public menuCtrl: MenuController,
    public hospitalCard: HospitalCard,
    public storage: Storage
  ) {
    setTimeout(() => {
      this.slides.lockSwipes(true);
        Auth.currentUserPoolUser()
          .then((response: any) => {
            console.log('response', response)
            if (response && response.signInUserSession && response.signInUserSession.idToken && response.signInUserSession.idToken.payload && response.signInUserSession.idToken.payload.nickname) {
              this.nickname = response.signInUserSession.idToken.payload.nickname;
            }
          })
          .catch((err: any) => {
            console.log('err occured', err)
          })
    });
    this.menuCtrl.swipeEnable(false);
    this.menuCtrl.enable(false);
    this.helper.getUid()
      .then((value: any) => {
        this.uid = value;
      });
  }

  slideToPrev() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  slideToNext() {
    let self: any = this;
    // slide to next slide, if not the last
    this.slides.lockSwipes(false);
    if (this.slides.getActiveIndex() >= 1) {
      this.launchSubscriptionListing();
    }
    else {
      if (this.slides.getActiveIndex() == 0) {
        this.helper.showLoading();
        Auth.currentUserPoolUser()
          .then((response: any) => {
            return Auth.updateUserAttributes(response, {
              nickname: self.nickname
            });
          })
          .then((response: any) => {
            return this.storage.set('name', self.nickname);
          })
          .then((response: any) => {
            self.helper.hideLoading();
            self.slides.slideNext();
            this.slides.lockSwipes(true);
          })
          .catch((err) => {
            self.helper.hideLoading();
            self.helper.showMessage(err.message, 3000);
            self.navCtrl.setRoot(LoginComponent)
            console.log('err occured', err)
          })
      }
      else {
        self.slides.slideNext();
        this.slides.lockSwipes(true);
      }
    }
  }

  updateOptions(data: any) {
    let self = this;
    console.log(data);
    _.each(_.keys(data), function(value: any) {
      self.valid[value] = data[value]
    })
  }

  isValid(form: NgForm) {
    if (this.slides.getActiveIndex() == 0) {
      return this.valid.security && form.valid;
    } else if (this.slides.getActiveIndex() == 1) {
      return this.valid.hospitals && this.valid.specialization;
    } else {
      return this.valid.subscription;
    }
  }

  launchSubscriptionListing() {
    this.navCtrl.push(SubscriptionListing, { isWelcomeScreen: true });
  }

  ngOnDestroy() {
    this.menuCtrl.swipeEnable(true);
    this.menuCtrl.enable(true);
  }

}
