import { Component, OnDestroy, OnInit } from "@angular/core";
import { Events, MenuController, NavController, NavParams, ViewController } from 'ionic-angular';
import { ForgotPasswordStepComponent } from "../forgot-password/forgot-password";
import { Keyboard } from '@ionic-native/keyboard';
import { RegisterComponent } from "../register/register";
import { RegistrationConfirmationComponent } from "../confirm-registration/confirm-registration";
import { HomePage } from '../../home/home';
import { WelcomePage } from '../../welcome/welcome';
import { Helper } from '../../../providers/helper';
import { SubscriptionHelperSerivce, SubscriptionListing } from '../../../subscriptions-module';
import { StorageService } from '../../../providers/storage-service';
import { Auth } from 'aws-amplify';
import { GraphqlService } from '../../../app/graphql.service';

const keyboard = window['Keyboard'];

@Component({
  selector: 'page-login',
  templateUrl: './login.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  registerPage: any = RegisterComponent;
  forgotPasswordPage: any = ForgotPasswordStepComponent;
  email: string;
  password: string;
  errorMessage: string;
  mfaStep = false;
  mfaData = {
    destination: '',
    callback: null
  };
  showFooter: boolean = true;

  constructor(
    public helper: Helper,
    public events: Events,
    public graphqlService: GraphqlService,
    // private keyboard: Keyboard,
    public navParams: NavParams,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public storageService: StorageService,
    public subscriptionHelper: SubscriptionHelperSerivce
  ) {
    this.menuCtrl.swipeEnable(false);
    this.menuCtrl.enable(false);
    if (keyboard) {
      keyboard.hideKeyboardAccessoryBar(true);
      keyboard.disableScroll(false);
    }

    if (this.navParams.get('email')) {
      this.email = this.navParams.get('email');
    }

    window.addEventListener('keyboardWillShow', (event) => {
      this.showFooter = false;
    });

    window.addEventListener('keyboardWillHide', () => {
      this.showFooter = true;
    });

  }

  ngOnInit() {
    this.errorMessage = null;
  }

  onLogin() {
    let self: any = this;
    this.errorMessage = null;
    this.helper.showLoading();
    let tempUser: any = {};
    Auth.signIn(this.email, this.password)
      .then((resposne: any) => {
        console.log('user logged in successfully', resposne);
        resposne.signInUserSession.idToken.payload
        tempUser.name = resposne.signInUserSession.idToken.payload.nickname;
        tempUser.email = resposne.username;
        tempUser.uid = resposne.signInUserSession.idToken.payload.sub;
        return this.graphqlService.hydrateClient();
      })
      .then(() => {
        console.log(tempUser)
        return this.helper.login(tempUser);
      })
      .then(() => {
        this.events.publish('user:login');
        this.helper.hideLoading();
        this.navigateUser();
      })
      .catch(err => {
        console.log('err occured', err)
        if (err.message == 'User is not confirmed.') {
          Auth.resendSignUp(this.email)
            .then((response: any) => {
              self.helper.hideLoading();
              self.navCtrl.push(RegistrationConfirmationComponent, { email: self.email });
            })
            .catch((err) => {
              console.log('send code error', err);
              self.helper.hideLoading();
              self.helper.showMessage(err.message, 3000);
            });
        }
        else {
          this.helper.hideLoading();
          this.helper.showMessage(err.message, 3000);
        }
      })
  }

  navigateUser() {
    let self: any = this;
    this.helper.storageService.getSettings('APP_READY')
      .then((value: any) => {
        if (value == "true") {
          this.helper.fetchUserCloudSettings()
            .then(() => {
              return self.storageService.getSettings('STRIPE_CUST_ID');
            })
            .then((stripeCustId: any) => {
              if (stripeCustId && stripeCustId != 'null') {
                self.subscriptionHelper.isSubscriptionActive(stripeCustId)
                  .then((subscriptionResponse: any) => {
                    if (subscriptionResponse.isExists) {
                      self.helper.hideLoading();
                      self.helper.showMessage('Logged-in successfully');
                      self.helper.getTopLookupCodes();
                      self.navCtrl.setRoot(HomePage);
                    }
                    else {
                      self.helper.hideLoading();
                      self.navCtrl.setRoot(SubscriptionListing);
                    }
                  })
              }
              else {
                self.helper.hideLoading();
                self.navCtrl.setRoot(WelcomePage);
              }

            })
        } else {
          self.helper.hideLoading();
          self.navCtrl.setRoot(WelcomePage);
        }
      })
  }

  ngOnDestroy() {
    this.menuCtrl.swipeEnable(true);
    this.menuCtrl.enable(true);
  }

}
