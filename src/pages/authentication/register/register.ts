import { Component } from "@angular/core";
import { ModalController, NavController, ViewController } from 'ionic-angular';
import { RegistrationConfirmationComponent } from "../confirm-registration/confirm-registration";
import { Helper } from '../../../providers/helper';
import { TermsPage } from '../../../components/terms-policy/terms';
import { PolicyPage } from '../../../components/terms-policy/policy';
import { Auth } from 'aws-amplify';

export class RegistrationUser {
  name: string;
  email: string;
  phone_number: string;
  password: string;
}
/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './register.html'
})
export class RegisterComponent {
  registrationUser: RegistrationUser;

  errorMessage: string;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public helper: Helper
  ) {
    this.onInit();
  }

  onInit() {
    this.registrationUser = new RegistrationUser();
    this.errorMessage = null;
  }

  onRegister() {
    this.helper.showLoading();
    Auth.signUp(this.registrationUser.email, this.registrationUser.password, this.registrationUser.email)
      .then(response => {
        this.helper.hideLoading();
        this.navCtrl.push(RegistrationConfirmationComponent, { email: this.registrationUser.email });
      })
      .catch(err => {
        this.helper.hideLoading();
        this.helper.showMessage(err.message, 3000);
        console.log('err occured', err);
      })
  }

  viewTerms(link: any) {
    let page = link == 'terms' ? TermsPage : PolicyPage;
    let modal = this.modalCtrl.create(page);
    modal.present();
  }
}
