import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CognitoUserAttribute } from "amazon-cognito-identity-js";
import * as _ from 'underscore';
import { Helper } from '../../../providers/helper';
import { Auth } from 'aws-amplify';
import { LoginComponent } from '../../authentication/login/login';

@Component({
  selector: 'edit-account',
  templateUrl: 'edit-account.html'
})
export class EditAccountPage {

  user: any = {};
  tempName: any = {};
  msg: any = '';
  updateAccountPage: any;

  constructor(
    public storage: Storage,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public helper: Helper
  ) {
    this.updateAccountPage = navParams.get('updateAccountPage');
    this.helper.storageService.getSettings('name')
      .then((name: any) => {
        this.user.name = name;
        this.tempName = name;
      })
      .catch((err: any) => {
        console.log('Err occured ', err);
      });
  }

  isValid() {
    return (this.user.oldPassword && this.user.newPassword) || (this.tempName != this.user.name);
  }

  update() {
    let self: any = this;
    this.helper.showLoading();
    Auth.currentUserPoolUser()
      .then((userResponse: any) => {
        new Promise((resolve) => {
          if (self.tempName != self.user.name) {
            Auth.updateUserAttributes(userResponse, {
              nickname: self.user.name
            })
              .then(() => {
                self.msg = 'Name ';
                return self.helper.storageService.setLocalSettings('name', self.user.name)
              })
              .then(() => {
                resolve();
              });
          }
          else {
            resolve();
          }
        })
          .then(() => {
            if (!_.isEmpty(self.user.oldPassword) || !_.isEmpty(self.user.newPassword)) {
              if (self.user.oldPassword.length < 6 || self.user.newPassword.length < 6) {
                self.helper.showMessage('Passwords do not match');
              }
              else {
                Auth.changePassword(userResponse, self.user.oldPassword, self.user.newPassword)
                  .then((response: any) => {
                    self.msg = self.msg == 'Name ' ? self.msg + ' and Password ' : 'Password '
                    console.log('call result: ', response);
                    self.helper.hideLoading();
                    self.helper.showMessage(self.msg + ' updated successfully');
                    self.updateAccountPage();
                    self.viewCtrl.dismiss();
                  })
                  .catch((err) => {
                    console.log('changePassword err ', err);
                    self.helper.hideLoading();
                    self.helper.showMessage(err.message);
                    self.user.oldPassword = self.user.newPassword = '';
                    return;
                  });
              }
            }
            else {
              if (self.tempName != self.user.name) {
                self.tempName = self.user.name;
                self.msg = !self.msg ? 'Name ' : 'Password ';
                self.updateAccountPage();
                self.helper.hideLoading();
                self.helper.showMessage(self.msg + ' updated successfully');
                self.viewCtrl.dismiss();
              }
            }
          })
      })
      .catch((err) => {
        this.helper.hideLoading();
        self.helper.showMessage('Please re-login to continue');
        this.navCtrl.setRoot(LoginComponent)
      })

  }

}
