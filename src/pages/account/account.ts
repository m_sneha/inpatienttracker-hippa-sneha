import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { EditAccountPage } from './edit-account/edit-account';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  user: any = {}

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public modalCtrl: ModalController

  ) {
    this.init();
  }

  init() {
    this.storage.get('name')
      .then((name: any) => {
        this.user.name = name;
        return this.storage.get('email')
      })
      .then((email: any) => {
        this.user.email = email;
      })
      .catch((err: any) => {
        console.log('Err occured ', err);
      });
  }

  edit() {
    let modal = this.modalCtrl.create(EditAccountPage, { updateAccountPage: () => { this.init() } });
    modal.present();
  }

}
