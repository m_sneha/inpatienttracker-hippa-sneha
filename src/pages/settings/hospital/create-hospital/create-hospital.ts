import { Component } from '@angular/core';
import { ModalController, ViewController } from 'ionic-angular';
import { Helper } from '../../../../providers/helper';
import { AddHospital } from '../add-hospital/add-hospital';
import { HospitalService } from '../../../../providers/hospitalService';
import * as moment from 'moment';

@Component({
  selector: 'create-hospital',
  templateUrl: 'create-hospital.html'
})

export class CreateHospital {
  hospital: any = {};

  constructor(
    public helper: Helper,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public hospitalService: HospitalService
  ) { }

  saveHospital() {
    // this.hospital.createdBy = this.uid;
    this.hospital.code = 'CARD_NOT_SUPPORTED';
    this.hospital.isCustom = 'true';
    this.hospital.hospitalId = moment().format('x');
    // this.hospital.createdDate = moment().format('');

    console.log('obj passigng ', this.hospital);
    this.hospitalService.createUserHospital(this.hospital)
      .then(() => {
        this.helper.showMessage('Hospital created successfully, and added to Custom Hospitals', 3000)
        let modal = this.modalCtrl.create(AddHospital, { hospitaldata: this.hospital });
        modal.present();
        modal.onDidDismiss((data: any) => {
          if (data) {
            console.log('onDidDismiss ', data);
            this.viewCtrl.dismiss();
          }
        });
      })
      .catch((err: any) => {
        console.log('err occured', err)
      })
  }

}
