import { Component } from '@angular/core';
import { ModalController, NavParams, NavController, ViewController } from 'ionic-angular';
import { Helper } from '../../../../providers/helper';
import { HospitalService } from '../../../../providers/hospitalService';
import { HospitalList } from '../hospital-list/hospital-list';

@Component({
  selector: 'add-hospital',
  templateUrl: 'add-hospital.html'
})

export class AddHospital {
  hospital: any = {
    shortname: ''
  };
  mailList: any = {
    to: '',
    cc: null,
    bcc: null
  };
  isEdit: boolean = false;

  constructor(
    public helper: Helper,
    public nav: NavParams,
    public hospitalService: HospitalService,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
  ) {
    if (this.nav.get('hospitaldata')) {
      this.hospital = this.nav.get('hospitaldata');
    }

    if(this.nav.get('hospitalId')) {
      this.hospital.hospitalId = this.nav.get('hospitalId');
      this.getMailingList();
    }
  }

  getMailingList() {
    this.hospitalService.getMailingInfo(this.hospital.hospitalId)
    .then((data:any) => {
      this.isEdit = true;
      this.mailList = data;
    })
  }

  chooseLocalHospital() {
    let modal = this.modalCtrl.create(HospitalList);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        this.hospital = data;
      }
    });
  }

  saveHospital() {
    // this.hospital.mailList = this.mailList;
    delete this.hospital.icon;
    delete this.hospital.isSelected;
    this.helper.showLoading();

    this.hospitalService.createMailingInfo(this.hospital.hospitalId, this.mailList, this.isEdit)
    .then(() => {
      this.isEdit = this.hospital.isCustom ? this.hospital.isCustom : this.isEdit;
      if(!this.isEdit) {
        return this.hospitalService.createUserHospital(this.hospital)
      } else if(this.hospital.isCustom) {
        return this.hospitalService.updateUserHospitals({hospitalId: this.hospital.hospitalId, isRemoved: "false"})
      }
    })
    .then(() => {
      this.helper.hideLoading();
      let msg =  this.isEdit ? 'updated': 'added';
      this.helper.showMessage('Hospital ' + msg + ' successfully');
      this.getMailingList();
      this.navCtrl.remove(0, this.navCtrl.getViews().length);
    })
    .catch(() => {
        this.helper.hideLoading();
        this.helper.showMessage('Error Saving Hospital');
    });
  }

}
