import { Component } from '@angular/core';
import { Events, ModalController, ViewController } from 'ionic-angular';
import { HospitalCard } from '../../../components/hospital-card/hospital-card';
import { AddHospital } from '../../../pages/settings/hospital/add-hospital/add-hospital';
import { HospitalService } from '../../../providers/hospitalService';
import { Helper } from '../../../providers/helper';
import * as _ from 'underscore';


@Component({
  selector: 'page-hospital',
  templateUrl: 'hospital.html'
})

export class HospitalPage {

  hospitals: any = [];
  hospitalLength: any = 0;

  constructor(
    public events: Events,
    public helper: Helper,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public hospitalCard: HospitalCard,
    public hospitalService: HospitalService
  ) {
    this.init();
    this.events.subscribe('get:hospitals', () => {
      this.init();
    });
  }

  init() {
    let self = this;
    this.helper.showLoading();
    this.hospitalService.getUserHospitals()
      .then((hospitals: any) => {
        this.hospitals = [];
        this.hospitalLength = 0;
        _.each(hospitals, function(hospital: any) {
          if (hospital.hospitalId && hospital.isRemoved != 'true') {
            self.hospitals.push(hospital);
            self.hospitalLength++;
          }
        })
        if (!this.hospitalLength) {
          this.helper.storageService.setLocalSettings('HOSPITAL_DATA', {});
        }
        this.helper.hideLoading();
      }).catch((err) => {
        console.log(err);
        this.helper.hideLoading();
      });
  }


  viewHospital(hospitalId: any, hospital: any) {
    let modal = this.modalCtrl.create(AddHospital, { hospitalId: hospitalId, hospitaldata: hospital });
    modal.present();
  }

  removeHospital(hospital: any) {
    this.helper.confirmPrompt('Delete Hospital', 'This process will delete all information associated with this hospital including patient information and cannot be recovered', 'Continue')
      .then(() => {
        this.helper.showLoading();
        return this.helper.purgeHospital(hospital.hospitalId);
      })
      .then(() => {
        if (hospital.isCustom == "true") {
          return this.hospitalService.updateUserHospitals({ hospitalId: hospital.hospitalId, isRemoved: "true" })
        } else {
          return this.hospitalService.removeUserHospital(hospital.hospitalId)
        }
      })
      .then(() => {
        let index = _.findIndex(this.hospitals, hospital);

        if (index > -1) {
          this.hospitals.splice(index, 1);
        }
        this.helper.hideLoading();
        this.init();
        this.helper.showMessage('Hospital deleted successfully');
      })
      .catch((err: any) => {
        this.helper.hideLoading();
        err == 'NO_ERROR' ? '' : this.helper.showMessage('Error removing hospital');
        console.log('Error removing hospital ', err);
      });
  }

}
