import { Component, ViewChild } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { HospitalPage } from './hospital/hospital';
import { SecurityPage } from './security/security';
import { Helper } from '../../providers/helper';
import * as moment from 'moment';
import { SpecializationPage } from './specialization/specialization';
import { SpecializationService } from '../../providers/specialization-service';
import { StorageService } from '../../providers/storage-service';
import  { TermsPage } from '../../components/terms-policy/terms';
import { PolicyPage } from '../../components/terms-policy/policy';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})

export class SettingsPage {

  hospitalPage: any = HospitalPage;
  securityPage: any = SecurityPage;
  specializationPage: any = SpecializationPage;
  selectedSpec: any;
  notificationTime: any;
  @ViewChild('dateTime') timepicker: any;
  moment = moment;

  constructor(
    public helper: Helper,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public service: SpecializationService,
    public storage: StorageService
  ) {
    this.helper.storageService.getSettings('NOTIFICATION_TIME')
      .then((time: any) => {
        this.notificationTime = time ? time : moment().startOf('day').add(17, 'hours').format('HH:mm');
      })
      .catch((err: any) => {
        console.log('err occured', err);
      });
      this.renderSpecialization();
  }

  viewTerms(link:any) {
    let page = link == 'terms' ? TermsPage : PolicyPage;

    let modal = this.modalCtrl.create(page);
    modal.present();
  }

  renderSpecialization() {
    this.storage.getSettings("SPECIALIZATION_ID")
      .then((specializationId: any) => {
        if(specializationId) {
          this.service.getSpecializationName(specializationId)
          .then((data:any) => {
              this.helper.getTopLookupCodes();
              this.selectedSpec = data.name;
          });
        }
      });
  }

  ionViewDidEnter() {
    this.renderSpecialization();
  }

  setNotificationTime(date: any) {
    this.helper.showLoading();
    this.helper.setNotificationTime(this.notificationTime)
      .then((res) => {
        let msg = res ? 'Notification Timer set successfully.' : 'Error occured while saving timer.';
        this.helper.hideLoading();
        this.helper.showMessage(msg);
      });
  }
}
