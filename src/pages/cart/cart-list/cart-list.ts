import { Component } from '@angular/core'
import { Helper } from '../../../providers/helper';
import { HospitalService } from '../../../providers/hospitalService';
import { CartDetail } from '../cart-detail/cart-detail';
import * as _ from 'underscore';

@Component({
  selector: 'cart-list',
  templateUrl: 'cart-list.html'
})

export class CartList {
  uid: any;
  hospitals: any = [];
  hospitalLength:any = 0;
  cartDetailPage: any = CartDetail;

  constructor(
    public helper: Helper,
    public hospitalService: HospitalService
  ) { }

  loadCartList() {
    this.helper.showLoading();
    this.hospitalService.getUserHospitals()
    .then((data:any) => {
      this.helper.hideLoading();
      this.hospitals = data;
      this.getLength();
      console.log('cart ', this.hospitals);
    });
  }

  getLength() {
    let self = this;
    _.each(this.hospitals, function(hospital:any) {
      if(hospital.hospitalId && parseInt(hospital.cartCount) && hospital.isRemoved != 'true') {
        self.hospitalLength++;
      }
    })
  }

  ionViewDidEnter() {
    this.loadCartList();
  }

}
