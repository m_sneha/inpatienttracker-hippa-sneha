import { Component, ViewChild } from '@angular/core';
import { NavParams, ModalController, AlertController, ActionSheetController, NavController, Slides, ItemSliding, Item } from 'ionic-angular';
import { Helper } from '../../providers/helper';
import { GetNotes } from '../../queries/entities';
import { AddEntitiyPage } from '../add-entity/add-entity';
import { AddPatientPage } from '../add-patient/add-patient';
import { PatientService } from '../../providers/patient-service';
import { Keyboard } from '@ionic-native/keyboard';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import * as _ from 'underscore';

@Component({
  selector: 'page-patient-details',
  templateUrl: 'patient-details.html'
})

export class PatientDetailPage {

  @ViewChild(Slides) slides: Slides;

  patient: any = {};
  items: any = { notes: [], diagnosis: [], procedures: [], em_code: [] };
  groupedItems: any = { notes: [], diagnosis: [], procedures: [], em_code: [] };
  recordSettings: any = {
    notes: { name: 'notes', key: 'notes', index: 0, isEnabled: true },
    diagnosis: { name: 'diagnosis', key: 'icdCodes', index: 1, isEnabled: true },
    em_code: { name: 'em_code', key: 'visitCodes', index: 2, isEnabled: true },
    procedures: { name: 'procedures', key: 'cptCodes', index: 3, isEnabled: true },
  };
  hospitalId: string;
  uid: any;
  patientDataSubscription: any;
  patientEntityDetails: any;
  showFooter: boolean = true;
  apolloService: any;
  notesBackgroundColors = ['khaki', '#c9edcc', '#f7d1c5', '#B2DFDB']
  moment = moment;
  isFromHomepage: boolean;
  notesQueryRef: any;
  entitiesQueryRef: any;
  isNotesLoaded: any = false;
  areEntitiesLoaded: any = false;
  isSavableRecordExist: boolean = false;
  notesObserver: Observable<any>;
  updateHomepageCallback: any;
  _ = _;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public modal: ModalController,
    public params: NavParams,
    public helper: Helper,
    private keyboard: Keyboard,
    public patientService: PatientService,
    public storage:Storage
  ) {
    this.hospitalId = this.params.get("hospitalId");
    this.apolloService = this.patientService.apolloService;
    this.patient = this.params.data.patient;
    this.updateHomepageCallback = this.params.get("updateHomepageCallback");
    this.isFromHomepage = !!this.updateHomepageCallback;
    this.storage.get('SPECIALIZATION_ID')
    .then((specializationId)=>{
      if(specializationId=='6'){
        this.recordSettings.procedures.isEnabled = false;
      }
    })
    this.getNotes();
    this.getPatientEntities();
    this.keyboard.onKeyboardShow()
      .subscribe(() => {
        this.showFooter = false;
      });

    // show footer on keyboard hide
    this.keyboard.onKeyboardHide()
      .subscribe(() => {
        this.showFooter = true;
      });
  }

  groupByDate(entities: any) {
    return _.groupBy(entities, (eachItem: any) => { return moment(eachItem.date, 'YYYYMMDD').format('MM/DD/YYYY') });
  }

  getCategoryTitle(type) {
    if (type == 'em_code') {
      return "E/M Code"
    }
    else if (type == 'diagnosis') {
      return "DIAGNOSES"
    }
    else {
      return type.toUpperCase();
    }
  }

  showSlideOptions(itemSlide: ItemSliding, item: Item) {
    // reproduce the slide on the click
    itemSlide.setElementClass("active-sliding", true);
    itemSlide.setElementClass("active-slide", true);
    itemSlide.setElementClass("active-options-right", true);
    item.setElementStyle("transform", "translate3d(-130px, 0px, 0px)");
  }

  hideSlideOptions(itemSlide: ItemSliding) {
    // reproduce the slide on the click
    if (itemSlide) {
      itemSlide.setElementClass("active-sliding", false);
      itemSlide.setElementClass("active-slide", false);
      itemSlide.setElementClass("active-options-right", false);
    }
  }

  async getPatientEntities() {
    this.helper.showLoading("Loading patient details");
    let self = this;
    let patientEntities: any = await this.patientService.getPatientEntities(this.entitiesQueryRef, this.patient.patientId, this.hospitalId);
    patientEntities
      .take(1)
      .subscribe((data) => {
        data = data.getDiagnosisAndBillingForPatient.items;
        data = JSON.parse(JSON.stringify(data[0] || {}));
        if (data) {
          _.keys(this.items).forEach((entityType: any) => {
            if (entityType != 'notes') {
              _.each(((data.billDetails || {})[entityType]) || [], (eachEntitiy) => { delete eachEntitiy.__typename });
              self.items[entityType] = (data.billDetails || {})[entityType] || [];
            }
            this.isNotesLoaded ? this.helper.hideLoading() : '';
          });
          this.patientEntityDetails = data;
          this.patientEntityDetails.updatedOn = moment.utc().format('YYYYMMDD');
          this.areEntitiesLoaded = true;
          this.groupItems();
        }
        else {
          this.helper.hideLoading();
        }
      })
  }

  async getNotes() {
    try{
      let notesObserver = await this.apolloService.fetch(this.notesQueryRef, GetNotes, { patientId: this.patient.patientId });
      notesObserver
        .take(1)
        .subscribe((data: any) => {
          data = data.getUserNotesForPatient.items;
          this.items.notes = data.length ? JSON.parse(JSON.stringify(data)) : [];
          this.isNotesLoaded = true;
          this.areEntitiesLoaded ? this.helper.hideLoading() : '';
      }, (err: any) => { console.log(err) })
    } catch(err){
      this.helper.hideLoading();
      console.log(err);
    }
  }

  getAllEntities() {
    let self = this;
    return _.sortBy(_.keys(this.items), function(type: any) {
      return (self.recordSettings[type] || {}).index;
    });
  }

  getNotesBackgroundColor(index: number) {
    let color = this.notesBackgroundColors[index % this.notesBackgroundColors.length];
    return { backgroundColor: color };
  }

  openNotes(notes?: any, mode?: string) {
    let modal = this.modal.create(AddEntitiyPage, {
      type: 'notes',
      notes: notes || null,
      patientId: this.patient.patientId,
      admissionDate: this.patient.dateOfAdmission,
      patientServiceRef: this.patientService,
      mode: mode || false,
      notesCallback: (noteData: any) => { this.updateNotes(noteData) }
    });
    modal.present();
  }

  updateNotes(noteData) {
    noteData = Object.assign({}, noteData);
    let index = _.findIndex(this.items.notes, { id: noteData.id });
    if (index > -1) {
      this.items.notes[index] = noteData
    } else {
      this.items.notes.push(noteData);
    }
  }

  async selectEditActions(myEvent: any, note: any) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Edit Note',
          icon: 'md-create',
          handler: () => {
            this.openNotes(note, 'EDIT');
          }
        },
        {
          text: 'Delete Note',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            this.helper.confirmPrompt('Confirm Delete', `Are you sure to Delete the note?`, 'Continue')
              .then(async () => {
                this.helper.showLoading();
                try{
                  let deleteNote: any = await this.patientService.deletePatientNotes({ id: note.id, patientId: this.patient.patientId, hospitalId: this.hospitalId });
                  deleteNote = deleteNote.deleteUserNotes;
                  let noteIndex = _.findIndex(this.items.notes, { id: deleteNote.id });
                  this.items.notes.splice(noteIndex, 1);
                  this.helper.hideLoading();
                  this.slides.update();
                  this.slides.slideTo(0);
                  this.helper.showMessage(`Note with title ${note.title} successfully got deleted`, 2000, 'toast-success');
                } catch(err){
                    this.helper.hideLoading();
                    console.log(err)
                }
              })
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  async addToCart() {
    try {
      this.helper.showLoading();
      await this.patientService.upsertPatient({ "patientId": this.patient.patientId, "hospitalId": this.patient.hospitalId, "status": "CART" });
      await this.upsertEntities();
      this.helper.hideLoading();
      this.helper.showMessage('Patient added for billing');
      this.helper.getInCartPatients(this.patient.hospitalId)
    }
    catch (err) {
      console.log(err);
      this.helper.hideLoading();
    }
  }

  groupItems() {
    let self = this;
    _.each(_.keys(this.items), (entityType: any) => {
      self.items[entityType] = _.sortBy((self.items[entityType] || []), (eachItem: any) => { return +moment(eachItem.date).format('x') });
      if (entityType == 'notes') {
        self.items[entityType] = self.items[entityType].reverse();
      }
      else {
        this.groupedItems[entityType] = _.groupBy(_.sortBy(self.items[entityType] || [], (eachEntity) => {
          return eachEntity.name;
        }), (eachItem: any) => { return eachItem.date });
      }
    })
  }

  openEntitiesList(type: string, items?: any[], mode?: boolean, slidingItem?: ItemSliding) {
    let modal = this.modal.create(AddEntitiyPage,
      {
        type: type,
        patientServiceRef: this.patientService,
        mode: mode || null,
        admissionDate: this.patient.dateOfAdmission,
        items: items,
        patientId: this.patient.patientId,
      });

    type = (type.toLowerCase());
    modal.onDidDismiss((data: any) => {
      if (data) {
        if (mode) {
          let entities = _.clone(this.items[type]);
          entities = _.filter(entities, ((entity: any) => {
            return entity.date == data[0].date;
          }))
          entities.forEach((entity: any) => {
            let index = _.findIndex(this.items[type], entity);
            if (index != -1) {
              this.items[type].splice(index, 1);
            }
          })
        }
        data = data.length ? data : [data];
        let count = 0;
        data.forEach((eachEntity: any) => {
          this.items[type] = (this.items[type] || []);
          let isEntityExist = _.findIndex(this.items[type], { name: eachEntity.name, code: eachEntity.code, date: eachEntity.date }) > -1 ? true : false;
          if (isEntityExist) {
            count++;
          }
          else {
            this.items[type].push(eachEntity)
            this.isSavableRecordExist = true;
          }
        })
        count > 0 ? this.helper.showMessage(`${count} Duplicate item(s) were ignored `) : '';
        data[0].title ? this.helper.showMessage(`Note with title ${data[0].title} successfully got added`, 2000, 'toast-success') : '';
        this.groupItems();
      }
    })
    modal.present();
  }

  deleteEntities(type: string, items: any[], slidingItem: ItemSliding) {
    this.helper.confirmPrompt('Confirm Delete', `Are you sure to Delete this record from ${type}?`, 'Continue')
      .then(() => {
        type = (type.toLowerCase());
        let count = 0;
        items.forEach((item: any) => {
          let index = _.findIndex(this.items[type], item);
          let entity = this.items[type][index];
          if (index != -1 && (entity.isBilled != "true")) {
            this.items[type].splice(index, 1)
            this.isSavableRecordExist = true;
          }
          else {
            count++;
          }
          this.hideSlideOptions(slidingItem);
          slidingItem.close();
        })
        count > 0 ? this.helper.showMessage(`${count} Billed item(s) were ignored `, 2000) : this.helper.showMessage(`Record(s) from ${type} were removed`, 2000);
        this.groupItems();
      })
      .catch((err) => {
        console.log(err);
        this.hideSlideOptions(slidingItem);
        slidingItem.close();
      })
  }

  ngOnDestroy() {
    this.isSavableRecordExist ? this.upsertEntities() : '';
    this.notesQueryRef ? this.notesQueryRef.unsubscribe() : '';
    this.entitiesQueryRef ? this.entitiesQueryRef.unsubscribe() : '';
  }

  upsertEntities() {
    return new Promise(async (resolve, reject) => {
      try {
        let entities = _.clone(this.items);
        delete entities.notes;
        if (this.patientEntityDetails) {
          this.patientEntityDetails.patientId = this.patient.patientId;
          this.patientEntityDetails.billDetails = _.clone(entities);
        }
        else {
          this.patientEntityDetails = {
            patientId: this.patient.patientId,
            billDetails: _.clone(entities)
          };
        }
        this.patientEntityDetails.createdOn = this.patientEntityDetails.createdOn || moment.utc().format('YYYYMMDD');
        this.patientEntityDetails.updatedOn = moment.utc().format('YYYYMMDD');
        let upsertVariables = { ...this.patientEntityDetails, ...{ hospitalId: this.hospitalId } };

        await this.patientService.upsertPatientEntities(upsertVariables);

        let areActiveItemsAvailable: boolean = this.checkForActiveEntities();
        if (areActiveItemsAvailable) {
          this.patient.lastUpdated = moment().format('x');
          await this.patientService.markVisitedPatient(this.patient.patientId, this.patient.lastUpdated, this.hospitalId)
          this.updateHomepageCallback(this.patient, "UPDATE");
        }
        resolve();
      } catch (err) {
        console.log(err);
        reject(err);
      }
    })
  }

  removeItem(type: string, item: any) {
    let alert = this.alertCtrl.create({
      title: 'Sure to delete ?',
      message: `Delete ${type.toLowerCase()} - ${item.name}`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Delete',
          handler: () => {
            let index = _.findIndex(this.items[type], item);
            if (index != -1) {
              this.items[type].splice(index, 1);
            };
          }
        }
      ]
    });
    alert.present();
  }

  deletePatientPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Delete Patient ?',
      message: `Sure to delete patient - ${this.patient.name}`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.deletePatient();
          }
        }
      ]
    });
    alert.present();
  }

  async deletePatient() {
    try {
      this.helper.showLoading("Deleting patient");
      let queryParams = `?hospitalId=${this.hospitalId}&deleteKey=patient&patientId=${this.patient.patientId}`;
      await this.helper.callDeleteApi(queryParams);
      this.helper.getInCartPatients(this.patient.hospitalId);
      this.updateHomepageCallback ? this.updateHomepageCallback(this.patient, "DELETE") : '';
      this.helper.hideLoading();
      this.navCtrl.pop();
      this.helper.showMessage("Patient deleted");
    } catch (err) {
      console.log(err);
      this.helper.hideLoading();
      this.helper.showMessage("Error while deleting patient");
    }
  }

  async billEntities(billStatus) {
    let allEntities = _.clone(JSON.parse(JSON.stringify(this.items)));
    let patientDetails = this.getPatientDetails(billStatus);
    let billableEntities: any = {};

    delete allEntities.notes;
    _.each(_.keys(allEntities), (entityType: any) => {
      _.each(allEntities[entityType], (entity) => {
        if (entity.isBilled == 'false' || entity.isBilled == false) {
          billableEntities[entityType] = billableEntities[entityType] || [];
          billableEntities[entityType].push(entity);
          entity.isBilled = 'true';
        }
      })
    })

    let upsertObj = {
      billDetails: allEntities,
      hospitalId: this.hospitalId,
      patientId: patientDetails.patientId,
      updatedOn: moment.utc().format('YYYYMMDD')
    };
    try{
      this.helper.showLoading("Billing Items");
      await this.patientService.upsertPatientEntities(upsertObj);
      await this.helper.sendRecord(patientDetails, billableEntities, billStatus.status);
      this.items = { ...{ notes: this.items.notes }, ...allEntities };
      let res: any = await this.patientService.upsertPatient(patientDetails);
      this.helper.hideLoading();
      this.isSavableRecordExist = false;
      this.helper.getInCartPatients(this.patient.hospitalId)
      if (billStatus.status == "DISCHARGED") {
        this.helper.showMessage("Patient discharged successfully");
        this.navCtrl.pop();
        this.updateHomepageCallback ? this.updateHomepageCallback(res.updatePatientDetail, "DELETE") : '';
      }
      else if (billStatus.status == "CART") {
        this.helper.showMessage("Patient added to cart");
      }
      else {
        this.groupItems();
        this.patient.lastUpdated = moment().format('x');
        await this.patientService.markVisitedPatient(this.patient.patientId, this.patient.lastUpdated, this.hospitalId)
        this.updateHomepageCallback(this.patient, "UPDATE");
        this.helper.showMessage("Patient billed successfully");
      }
    } catch(err){
      this.helper.showMessage("Error occured while billing");
      this.helper.hideLoading();
      console.log(err);
    }
  }

  getPatientDetails(billStatus) {
    let patientInfo = { ...this.patient, ...billStatus };
    patientInfo.dateOfBirth = moment(patientInfo.dateOfBirth).format('MM/DD/YYYY');
    patientInfo.dateOfAdmission = moment(patientInfo.dateOfAdmission).format('MM/DD/YYYY');
    return patientInfo;
  }

  deleteRecords() {
    let alert = this.alertCtrl.create({
      title: 'Delete records ?',
      message: `Sure to delete all records for ${this.patient.name}`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          handler: () => {
            this.deleteUnbilledRecords();
          }
        }
      ]
    });
    alert.present();
  }

  deleteUnbilledRecords() {
    this.isSavableRecordExist = false;
    let areActiveItemsExist: boolean = false, self = this;
    _.each(_.keys(this.items), (entityType) => {
      if (entityType != 'notes') {
        self.items[entityType] = _.filter(self.items[entityType], (eachEntity) => {
          if (!JSON.parse(eachEntity.isBilled)) {
            areActiveItemsExist = true;
          }
          return JSON.parse(eachEntity.isBilled);
        })
      }
    })

    if (areActiveItemsExist) {
      this.helper.showLoading();
      this.groupItems();
      let recordItems = { ...this.items };
      delete recordItems.notes;
      let variableObj = {
        patientId: this.patient.patientId,
        hospitalId: this.hospitalId,
        updatedOn: this.patientEntityDetails.updatedOn,
        billDetails: recordItems
      };
      this.patientService.upsertPatientEntities(variableObj)
        .then((res) => {
          this.helper.hideLoading();
          this.helper.showMessage('Patient records deleted');
        })
        .catch((err) => {
          this.helper.hideLoading();
          console.log(err);
        });
    }
    else {
      this.helper.showMessage('No patient records available to delete !');
    }
  }

  bill() {
    let areActiveItemsAvailable: boolean = this.checkForActiveEntities();
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Send for billing',
          handler: () => {
            areActiveItemsAvailable ? this.billEntities({ status: 'ACTIVE' }) : this.helper.showMessage("No billable records are added");
          }
        },
        {
          text: 'Bill later',
          handler: () => {
            areActiveItemsAvailable ? this.addToCart() : this.helper.showMessage("No billable records are added");
          }
        },
        {
          text: 'Send for bill & discharge',
          handler: () => {
            if ((this.checkForEntities()).arePresent) {
              this.getFollowup();
            } else {
              this.helper.showMessage("No records are present to discharge");
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
  }

  checkForActiveEntities() {
    console.log((this.checkForEntities()).areActive)
    return (this.checkForEntities()).areActive;
  }

  checkForEntities() {
    let areActive: boolean, arePresent: boolean;
    let self = this;
    _.each(_.keys(this.items || []), function(type: any) {
      if ((self.recordSettings[type] || {}).isEnabled) {
        if (type != 'notes') {
          _.each((self.items[type] || []), function(eachEntity: any) {
            arePresent = true;
            if (!JSON.parse(eachEntity.isBilled)) {
              areActive = true;
            }
          })
        }
      }
    });
    return { areActive: areActive || false, arePresent: arePresent || false };
  }

  getFollowup() {
    let alert = this.alertCtrl.create({
      title: 'Follow up in weeks',
      inputs: [
        {
          name: 'followUp',
          type: 'number',
          value: '2'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: (data: any) => {
            let billStatus = { followUpInWeeks: data.followUp + ' week(s)', dischargeDate: moment().format('YYYY-MM-DD'), status: 'DISCHARGED' }
            this.billEntities(billStatus);
          }
        }
      ]
    });
    alert.present();
  }

  showOptions() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Edit Patient',
          icon: 'md-create',
          handler: () => {
            this.navCtrl.push(AddPatientPage, { patient: this.patient, hospitalId: this.hospitalId, mode: "EDIT" });
          }
        },
        {
          text: 'Delete Patient',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            this.deletePatientPrompt();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  getCurrentSlideIndex() {
    if (!this.items.notes.length || !this.slides) {
      return 0;
    }
    let index = this.slides.getActiveIndex() + 1;
    return index > this.items.notes.length ? this.items.notes.length : index;
  }

  getActiveIndex() {
    return this.slides ? this.slides.getActiveIndex() : 0;
  }
}
