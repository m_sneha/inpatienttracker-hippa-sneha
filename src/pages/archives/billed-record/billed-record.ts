import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { Helper } from '../../../providers/helper';
import * as _ from 'underscore';
import * as moment from 'moment';
import { PatientService } from '../../../providers/patient-service';

@Component({
  selector: 'page-billed-record',
  templateUrl: 'billed-record.html'
})

export class BilledRecordPage {

  patientDetails: any = {};
  diagnosis: any[] = [];
  em_code: any[] = [];
  procedures: any[] = [];
  currentHospital: any = {};
  items: any = { diagnosis: [], procedures: [], em_code: [] };
  _ = _;
  moment = moment;
  patientEntitiesRef: any;
  patient: any;
  groupedItems: any = { em_code: {}, procedures: {}, diagnosis: {} };
  types: any = ['em_code', 'procedures', 'diagnosis'];
  updateArchivePageCallback: any;
  isIOS: any;
  constructor(
    public alertCtrl: AlertController,
    public helper: Helper,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public patientService: PatientService
  ) {
    let self = this;
    this.isIOS = platform.is('ios');
    this.helper.showLoading();
    this.patient = this.navParams.get('patient');
    this.updateArchivePageCallback = this.navParams.get("updateArchivePageCallback");
    this.currentHospital = this.navParams.get('hospital');
    this.patientService.getPatientEntities(this.patientEntitiesRef, this.patient.patientId, this.currentHospital.hospitalId)
      .then((response: any) => {
        response
          .subscribe((data: any) => {
            if (data.getDiagnosisAndBillingForPatient.items && data.getDiagnosisAndBillingForPatient.items.length && data.getDiagnosisAndBillingForPatient.items[0]) {
              self.items = JSON.parse(JSON.stringify(data.getDiagnosisAndBillingForPatient.items[0].billDetails));
              self.groupItems(self.items);
              this.helper.hideLoading();
            }
          })
      })
  }

  cancelBilling() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Cancellation',
      message: 'Are you sure to cancel this record from Billing?',
      buttons: [{
        text: 'NO',
        role: 'cancel'
      }, {
        text: 'YES',
        handler: () => {
          let patient: any = Object.assign({}, this.patient);
          patient.status = 'ACTIVE';
          this.patientService.upsertPatient(patient)
            .then(() => {
              this.patient = patient.status;
              this.helper.showMessage('Record successfully got cancelled', 3000);
              this.navCtrl.pop();
              this.updateArchivePageCallback(patient.patientId, "DELETE");
            });
        }
      }]
    })
    alert.present();
  }

  mailReport(patient: any) {
    this.helper.showLoading("Preparing to mail report");
    let tempObj = Object.assign({}, patient);
    this.helper.sendRecord(tempObj, this.items)
      .then((response: any) => {
        console.log(response);
        this.helper.hideLoading();
        // this.helper.showMessage('Mail sent successfully', 3000, 'toast-success');
      })
      .catch((err) => {
        this.helper.hideLoading();
        this.helper.showMessage('Error occured while mailing', 3000);
        console.log(err);
      });
  }

  ngOnDestroy() {
    this.patientEntitiesRef ? this.patientEntitiesRef.unsubscribe() : '';
  }

  groupItems(items: any) {
    return new Promise((resolve) => {
      this.types.forEach((type: any) => {
        let object = _.groupBy((items[type] || []), (eachItem: any) => { return eachItem.date })
        this.groupedItems[type] = object;
      });
      resolve();
    })
  }
}
