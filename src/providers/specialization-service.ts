import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Apollo } from 'apollo-angular';
import { Helper } from '../providers/helper';
import { getSpecialization, listAllSpecializations } from '../queries/list-specializations';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';

@Injectable()

export class SpecializationService {

  specializations = new BehaviorSubject([]);
  name: any;

  constructor(
    private apollo: Apollo,
    public helper: Helper
  ) {
    // this.findAll();
  }

  findAll() {
    this.apollo.watchQuery({ query: listAllSpecializations, fetchPolicy: 'network-only' })
      .valueChanges
      .pipe(map(result => result.data['listAllSpecializations'].items))
      .subscribe(o => {
        const copied = Object.assign([], o);
        this.specializations.next(copied);
        console.log(copied);
      }, () => {
      });
  }

  getSpecializationName(id:any) {
    return new Promise((resolve, reject) => {
      this.apollo.watchQuery({ query: getSpecialization, variables: { specializationId: id }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getSpecialization'].items)).subscribe(data => {
          resolve(data[0]);
        });
    })
  }

}
