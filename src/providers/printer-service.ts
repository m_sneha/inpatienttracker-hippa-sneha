import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import * as _ from 'underscore';
import * as moment from 'moment';
let jsPDF = require('jspdf');
require('jspdf-autotable');

@Injectable()

export class PrinterService {

  constructor(
    public platform: Platform,
    public file: File
  ) { }

  openPdf(name: any) {
    return new Promise((resolve) => {
      let doc = new jsPDF('p', 'pt', 'letter'),
        d = new Date(),
        directory = (this.platform.is('ios') ? this.file.dataDirectory : this.file.externalRootDirectory),
        fileName = name + '-' + ((d.getMonth() + 1) + '-' + d.getDate() + '-' + d.getFullYear()) + '_' + d.getHours() + d.getMinutes() + d.getSeconds() + '.pdf';

      resolve({
        fileName: fileName,
        doc: doc,
        directory: directory
      });
    })
  };

  headerCard(pdf: any, name: any) {
    return new Promise((resolve) => {
      pdf.autoTable(
        [{
          title: 'Doctor',
          dataKey: 'name'
        },], [{
          name: name
        }], {
          startY: 40,
          theme: 'grid',
          styles: {
            cellPadding: 5
          },
          headerStyles: {
            fontSize: 12,
            valign: 'middle',
            halign: 'right',
            fontStyle: 'bold'
          },
          bodyStyles: {
            fontSize: 15,
            valign: 'middle',
            halign: 'right',
            fillColor: 255,
            textColor: 0
          }
        });
      resolve();
    })
  };


  patientCard(pdf: any, patient: any) {
    return new Promise((resolve) => {
      var columns = [{
        title: 'col-1',
        dataKey: 'title'
      }, {
        title: 'col-2',
        dataKey: 'name'
      },];
      var rows = [{
        title: 'Name',
        name: patient.name || ''
      }, {
        title: 'MR/FIN',
        name: patient.medicalRecordNumber || ''
      }, {
        title: 'DOB',
        name: patient.dateOfBirth || ''
      }, {
        title: 'Age',
        name: patient.age || moment().diff(moment(patient.dateOfBirth, 'MM/DD/YYYY').format(''), 'years')
      }, {
        title: 'Sex',
        name: patient.sex || ''
      }, {
        title: 'Admitting Doctor',
        name: patient.referringDoctor || ''
      }, {
        title: 'Account No',
        name: patient.accountNumber || ''
      }, {
        title: 'Date of Admission',
        name: patient.dateOfAdmission || ''
      }, {
        title: 'Hospital',
        name: patient.hospital
      }];

      pdf.setFontSize(15);
      pdf.text('Patient Card', 40, 120);
      pdf.autoTable(columns, rows, {
        startY: 130,
        styles: {
          cellPadding: 1
        },
        headerStyles: {
          fontSize: 10
        },
        bodyStyles: {
          fontSize: 10,
          valign: 'middle'
        },
        drawHeaderRow: function() {
          return false;
        },
        columnStyles: {
          title: {
            fillColor: 255,
            textColor: 0,
            fontStyle: 'bold',
            columnWidth: 150
          },
          name: {
            columnWidth: 'wrap'
          }
        }
      });
      resolve();
    })
  };

  recordsTable(pdf: any, record: any, actionType?: any) {

    return new Promise((resolve) => {
      let startY: any = 280;
      let recordColumns: any = [{
        title: 'Date',
        dataKey: 'date'
      }, {
        title: 'Code',
        dataKey: 'code'
      }, {
        title: 'Name',
        dataKey: 'name'
      }];
      let categories: any = [{
        title: 'Diagnoses',
        name: 'diagnosis'
      }, {
        title: 'E/M Codes',
        name: 'em_code'
      }, {
        title: 'Procedures',
        name: 'procedures'
      }];

      pdf.setFontSize(11);
      pdf.text('Record#' + record.created, 40, startY);

      pdf.setFontSize(13);
      pdf.setFontStyle('bold');

      _.each(categories, function(category: any) {
        let allItems: any = JSON.parse(JSON.stringify(record.items[category.name] || []));
        if (allItems && allItems.length) {
          let newItems: any = [];
          _.each(allItems, function(items: any) {
            items.date = moment(items.date).format('MM/DD/YYYY');
            newItems = newItems.concat(items);
          });

          pdf.setTextColor(0);
          startY = startY + 30;
          if (newItems.length > 0) {
            pdf.text(category.title, 40, startY);

            pdf.autoTable(recordColumns, newItems, {

              theme: 'grid',
              startY: startY + 10,
              styles: {
                cellPadding: 5
              },
              headerStyles: {
                fontSize: 10
              },
              bodyStyles: {
                fontSize: 10,
                valign: 'middle'
              },
              columnStyles: {
                dateTime: {
                  columnWidth: 175
                },
                code: {
                  columnWidth: 100
                }
              },
              alternateRow: {
                fillColor: 255
              }
            });
            startY = startY + (newItems.length * 15) + 20 + 40;
          }
        }
      });
      if (actionType == 'DISCHARGED') {
        startY = startY + 10;
        record.followUpInWeeks = ((record.followUpInWeeks || '').toLowerCase() == 'none') || !record.followUpInWeeks ? '-' : record.followUpInWeeks;
        let columns: any = [{ title: 'Follow Up in Weeks', dataKey: 'followUp' }, { title: 'Confirmed by', dataKey: 'confirmedBy' }]
        let rows: any = [{ followUp: record.followUpInWeeks, confirmedBy: '' }]
        pdf.autoTable(columns, rows, {
          theme: 'grid',
          startY: startY + 10,
          styles: {
            cellPadding: 5
          },
          headerStyles: {
            fillColor: false,
            textColor: 0,
            lineWidth: 1,
            lineColor: 200,
            fontSize: 12,
          },
          bodyStyles: {
            fontSize: 10,
            valign: 'middle'
          },
          columnStyles: {
            followUp: {
              fontSize: 12,
              columnWidth: 250
            },
            confirmedBy: {
              columnWidth: 250
            }
          },
        });
        startY = startY + (rows.length * 15) + 20 + 30;
        pdf.setTextColor(0, 0, 0);
        pdf.setFontSize(11);
        pdf.setFontType('normal');
        startY = startY + 30;
        pdf.text('Date ______________________', 40, startY);
        resolve();
      } else {
        resolve();
      }
    });
  };

}
