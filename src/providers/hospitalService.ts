import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Apollo } from 'apollo-angular';
import { Helper } from '../providers/helper';
import { listHospitals } from '../queries/list-hospitals';
import { createUserHospitals, createMailingInfo, deleteUserHospitals, deleteMailingInfo, updateMailingInfo, getMailingInfo, subscriptionToUserHospitals, getUserHospital, updateUserHospitals } from '../queries/userHospitals';
import { hospitals } from '../queries/hospitals';
import { subscriptionToHospitals } from '../queries/subscription-to-hospitals';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';

@Injectable()

export class HospitalService {

  hospitals = new BehaviorSubject([]);
  userHospitals = new BehaviorSubject([]);

  constructor(
    private apollo: Apollo,
    public helper: Helper
  ) { }

  initApollo() {

    this.apollo.watchQuery({ query: listHospitals, fetchPolicy: 'network-only' }).valueChanges
      .pipe(map(result => result.data['listAllHospitals'].items)).subscribe(data => {
        const copied = JSON.parse(JSON.stringify(data));
        this.hospitals.next(copied);
      });

    this.apollo.watchQuery({ query: getUserHospital, fetchPolicy: 'network-only' }).valueChanges
      .pipe(map(result => result.data['getUserHospitals'].items)).subscribe(data => {
        if (data && data.length) {
          const copied = JSON.parse(JSON.stringify(data));
          this.userHospitals.next(copied);
        }
      });

    // this.apollo.subscribe({query: subscriptionToUserHospitals}).subscribe(o => {
    //   const hospital  = o.data.onCreateUserHospitals;
    //   if (this.userHospitals.getValue()[0].findIndex(p => p.hospitalId === hospital.hospitalId) === -1) {
    //     this.userHospitals.next([...this.userHospitals.getValue()[0], hospital]);
    //     // const copied = Object.assign([], this.userHospitals.getValue()[0]);
    //     // this.userHospitals.next(copied);
    //   }
    // });

    // this.apollo.subscribe({query: subscriptionToHospitals}).subscribe(o => {
    //   const hospital  = o.data.onCreateHospitals;
    //   console.log(this.hospitals.getValue(), this.hospitals.getValue().findIndex(p => p.id === hospital.id));
    //   if (this.hospitals.getValue().findIndex(p => p.id === hospital.id) === -1) {
    //     // this.hospitals.next([...this.hospitals.getValue(), hospital]);
    //     const copied = Object.assign([], this.hospitals.getValue()[0]);
    //     this.hospitals.next(copied);
    //     console.log('hospitals ', this.hospitals.getValue()[0]);
    //   }
    // });

  }

  public getUserHospitals() {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: getUserHospital, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getUserHospitals'].items)).subscribe(data => {
          if (data && data.length) {
            const copied = JSON.parse(JSON.stringify(data));
            resolve(copied);
          } else {
            resolve([]);
          }
        });
    })
  }



  // Mailing List for particular hospital
  getMailingInfo(hospitalId: any) {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: getMailingInfo, variables: { hospitalId }, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getMailingInfo'])).subscribe(data => {
          resolve(JSON.parse(JSON.stringify(data)));
        });
    })
  }


  createUserHospital(hospital: any) {
    hospital = _.pick(hospital, _.identity);
    this.userHospitals.next([[...this.userHospitals.getValue(), hospital]]);
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: createUserHospitals, variables: hospital })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital creation");
          reject();
        }));
    });
  }

  updateUserHospitals(hospital: any) {

    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: updateUserHospitals, variables: hospital })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in hospital update");
          reject();
        }));
    });
  }

  removeUserHospital(hospitalId: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteUserHospitals, variables: { hospitalId } })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in removeUserHospital");
          reject();
        }));
    });
  }

  removeMailingInfo(hospitalId: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: deleteMailingInfo, variables: { hospitalId } })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in deleteMailingInfo");
          reject();
        }));
    });
  }

  createMailingInfo(hospitalId: any, mailingInfo: any, isEdit: any) {
    let temp = {
      hospitalId: hospitalId,
      to: mailingInfo.to,
      cc: mailingInfo.cc,
      bcc: mailingInfo.bcc
    }
    Object.keys(temp).forEach((eachKey) => {
      !temp[eachKey] ? delete temp[eachKey] : '';
    })
    let mutationType = isEdit ? updateMailingInfo : createMailingInfo;

    console.log('befor mailing info upsert ', isEdit, temp);

    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: mutationType, variables: temp })
        .take(1)
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in createMailingInfo creation ", err);
          reject();
        }));
    });
  }

}
