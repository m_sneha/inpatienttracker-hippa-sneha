import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/take';

@Injectable()

export class ApolloService {
  constructor(
    private apollo: Apollo,
  ) { }

  mutate(mutationObject: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate(mutationObject).take(1)
        .subscribe((res) => {
          resolve(res.data);
        }, (err) => { reject(err) });
    })
  }

  fetch(queryRef: any, queryObject: any, variables: any) {
    return new Promise((resolve, reject) => {
      try {
        queryRef = this.apollo.watchQuery({ query: queryObject, variables: variables, fetchPolicy: 'network-only' });
        resolve(queryRef.valueChanges.pipe(map((result: any) => result.data)));
      } catch (err) {
        reject(err);
      }
      // queryRef.refetch()
      //   .then(() => {
      // }).catch((err: any) => {
      //   console.log(err);
      // });
    })
  }
}
