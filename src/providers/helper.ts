import { Injectable } from '@angular/core';
import { AlertController, Events, LoadingController, Platform, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Badge } from '@ionic-native/badge';
import { StorageService } from './storage-service';
import { PatientService } from './patient-service';
import { TouchID } from '@ionic-native/touch-id';
import { getUserHospital, updateUserHospitals } from '../queries/userHospitals';
import { Apollo } from 'apollo-angular';
import { File } from '@ionic-native/file';
import { EmailComposer } from '@ionic-native/email-composer';
import { PrinterService } from './printer-service';
import { getUserSettings } from '../queries/settings';
import { ApolloService } from './apollo-service';
import { getMailingInfo } from '../queries/userHospitals';
import { BulkBillingPatient, GetCartPatientEntities, UpdatePatient } from '../queries/patient-info';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AnalyticsService } from './analytics-service';
import { api } from '../environments/api-config';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';
import * as moment from 'moment';
import { Auth } from 'aws-amplify';

declare const cordova: any;
declare const PDFProtector: any;
@Injectable()
export class Helper {

  loaderInstance: any;
  patients: any = [];
  settings: any = {};
  patientEntities: any = [];
  shadowEntities: any = [];
  entitiyQueryRef: any;
  favEntitiesQueryRef: any
  iosFilePath: string = '';

  constructor(
    public alertCtrl: AlertController,
    public apollo: Apollo,
    public apolloService: ApolloService,
    public emailComposer: EmailComposer,
    public file: File,
    public http: Http,
    public loadingCtrl: LoadingController,
    public patientService: PatientService,
    public platform: Platform,
    public printerService: PrinterService,
    public badge: Badge,
    public storage: Storage,
    public toastCtrl: ToastController,
    public touchId: TouchID,
    public storageService: StorageService,
    public events: Events,
    public analyticsService: AnalyticsService,
    // public service: SpecializationService
  ) { }

  promptAlert(title: any, message: any, buttonText: any) {
    return new Promise((resolve, reject) => {
      let pdfPasswordPrompt = this.alertCtrl.create({
        title: title,
        message: message,
        cssClass: 'pdf-password',
        inputs: [
          {
            name: 'passcode',
            type: 'number'
          }],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              reject();
            }
          },
          {
            text: buttonText,
            handler: (data: any) => {
              resolve(data.passcode);
            }
          }
        ]
      });
      pdfPasswordPrompt.present();
    })
  }

  getVal(val: any) {
    return (val || '').trim();
  };

  getAddress(hospital: any) {
    return `${this.getVal(hospital.address1) ? this.getVal(hospital.address1) + ', ' : ''}
    ${this.getVal(hospital.address2) ? this.getVal(hospital.address2) + ',' : ''}
    ${this.getVal(hospital.address1) || this.getVal(hospital.address2) ? '<br>' : ''}
    ${this.getVal(hospital.city)} ${this.getVal(hospital.state)} - ${this.getVal(hospital.zipcode)}`;
  };

  // show Loading UI
  showLoading(title?) {
    this.loaderInstance = this.loadingCtrl.create({ "content": title || '' });
    this.loaderInstance.present();
  }

  // hide the Loading UI
  hideLoading() {
    if (this.loaderInstance) {
      this.loaderInstance.dismiss();
      this.loaderInstance = null;
    }
  }

  isFingerprintAvailable() {
    return new Promise((resolve, reject) => {
      if (this.platform.is('android')) {
        cordova.plugins.Fingerprint.isAvailable(
          (res: any) => {
            if (res == 'AVAILABLE') {
              resolve(res);
            }
            resolve(null);
          },
          (err: any) => { resolve(null) }
        )
      }
      else if (this.platform.is('ios')) {
        this.touchId.isAvailable()
          .then(
            (res) => { resolve('AVAILABLE') },
            (err) => { resolve(null) }
          );
      }
      else {
        resolve(null);
      }
    })
  }

  async sendRecord(patientInfo: any, billableEntities: any, actionType?: any) {
    try {
      let appUserName: any = await this.storage.get('name'),
        currentHospitalData: any = (await this.storage.get('HOSPITAL_DATA'));

      return new Promise((resolve, reject) => {
        let settings: any, self = this;
        if(currentHospitalData && currentHospitalData.hospitalDetail && currentHospitalData.hospitalDetail.name){
          patientInfo.hospital = currentHospitalData.hospitalDetail.name;
        }
        else if(currentHospitalData && currentHospitalData.name){
          patientInfo.hospital = currentHospitalData.name;
        }
        patientInfo.created = moment().format('MM/DD/YYYY');
        self.printerService.openPdf('Billing-report')
          .then((pdfProperties: any) => {
            settings = {
              pdf: pdfProperties.doc,
              fileName: pdfProperties.fileName,
              directory: pdfProperties.directory
            };
            return self.printerService.headerCard(settings.pdf, appUserName);
          })
          .then(() => {
            return self.printerService.patientCard(settings.pdf, patientInfo);
          })
          .then(() => {
            let patientData = { ...patientInfo, ...{ items: billableEntities } };
            return self.printerService.recordsTable(settings.pdf, patientData, actionType);
          })
          .then(() => {
            return this.writeFile(settings.directory, settings.fileName, settings.pdf);
          })
          .then(() => {
            return this.getMailList(currentHospitalData.hospitalId);
          })
          .then((emailList: any) => {
            let mailOptions = {
              subject: 'Billing Statement',
              body: 'Find the attached billing report'
            }
            return this.sendMail(emailList, this.platform.is('ios') && this.iosFilePath ? this.iosFilePath : settings.directory + settings.fileName, mailOptions);
          })
          .then((res) => {
            resolve(res);
          })
          .catch((err: any) => {
            reject(err);
            console.log('Error occured ', err);
          });
      });
    } catch (err) {
      console.log("Error occured while fetching data for reporting");
    }
  }

  sendMail(mailList: any, pathToPdf: any, options: any) {
    let emaildata: any = {
      to: mailList.to,
      cc: mailList.cc,
      bcc: mailList.bcc,
      attachments: [pathToPdf],
      subject: options.subject,
      body: options.body,
      isHtml: true
    };
    return this.emailComposer.open(emaildata);
  }

  writeFile(directory: any, fileName: any, pdf: any) {
    let self: any = this;
    return new Promise(async (resolve, reject) => {
      let PDF_PASSCODE = "";
      try {
         PDF_PASSCODE = await this.storageService.getSettings("PDF_PASSCODE") || "";
      } catch (err) {
        console.log(err);
      }
      this.file.writeFile(directory, fileName, pdf.output())
        .then((res) => {
          if (PDF_PASSCODE) {
            if (this.platform.is('ios')) {
              let pdf = PDFProtector.create({ path: res.nativeURL, passcode: PDF_PASSCODE }, function(pathToSecurePdf) {
                console.log('pathToSecurePdf', pathToSecurePdf);
                self.iosFilePath = pathToSecurePdf;
                resolve();
              }, function(error) {
                console.log('Failed to crete a secure PDF', error);
                reject(error);
              });
            }
            else if (this.platform.is('android')) {
              cordova.plugins.PDF_Protect.addPassword(res.nativeURL, PDF_PASSCODE,
                function(res: any) {
                  resolve();
                },
                function(error: any) {
                  reject(error);
                })
            }
          }
          else {
            resolve();
          }
        })
    })
  }

  getMailList(hospitalId: any) {
    return new Promise(async (resolve, reject) => {
      let mailList: any;
      try {
        mailList = await this.apolloService.fetch(mailList, getMailingInfo, { hospitalId: hospitalId });
        mailList
          .take(1)
          .subscribe((res: any) => {
            res.getMailingInfo ? resolve(Object.assign({}, res.getMailingInfo)) : reject("Unable to fetch hospital mailing info");
          })
      } catch (err) {
        console.log(err);
        reject("Unable to fetch hospital mailing info")
      }
    })
  }

  // bulkBilling
  // hospitalObj, [patientIds], actionType
  bulkBilling(patients: any, patientIds: any, hospital: any, actionType: any) {
    this.showLoading();
    this.patients = patients;

    this.storage.get('name')
      .then((name: any) => {
        this.settings.appUserName = name;
        return this.getPatientEntities(hospital.hospitalId, patientIds)
      })
      .then((patientEntities: any) => {
        console.log('patientEntities ', patientEntities);
        this.shadowEntities = Object.assign([], patientEntities);
        this.patientEntities = patientEntities;
        return this.printerService.openPdf('Batch-Billing-report');
      })
      .then((pdfProperties: any) => {
        this.settings.pdf = pdfProperties.doc;
        this.settings.fileName = pdfProperties.fileName;
        this.settings.directory = pdfProperties.directory;
        this.settings.actionType = actionType;
        this.settings.hospital = hospital;

        this.addPage();
      })

  }

  getPatientEntities(hospitalId: any, patientIds: any) {
    return new Promise(async (resolve, reject) => {
      try {
        let entityObj: any;
        entityObj = await this.apolloService.fetch(entityObj, GetCartPatientEntities, { hospitalId: hospitalId, patientIds: patientIds });
        entityObj
          .take(1)
          .subscribe((res: any) => {
            res.getMultiplePatientDiagnosis ? resolve(Object.assign([], JSON.parse(JSON.stringify(res.getMultiplePatientDiagnosis)))) : reject("Unable to fetch patient entities");
          });
      } catch (err) {
        console.log(err);
      }
    })
  }

  // Update All entities isBilled property to true
  billEntities() {
    return new Promise((resolve) => {
      _.each(this.shadowEntities, function(patient) {
        if (_.isObject(patient)) {
          delete patient.__typename
          _.each(patient, function(bill) {
            if (_.isObject(bill)) {
              delete bill.__typename;
              _.each(bill, function(item) {
                if (item.length) {
                  delete item.__typename
                  _.each(item, function(entity) {
                    if (_.isObject(entity)) {
                      entity.isBilled = 'true';
                      delete entity.__typename;
                    }
                  })
                }
              })
            }
          })
        }
      })
      resolve();
    })
      .then(() => {
        return this.updateBilledEntities(this.settings.hospital.hospitalId, this.shadowEntities)
      });
  }

  addPage() {
    if (this.patients && !this.patients.length) {
      this.hideLoading();
      this.billEntities()
        .then(() => {
          if ((this.platform.is('android') || this.platform.is('ios'))) {
            return this.pdfWrite()
          }
        })
        .then(() => {
          this.getInCartPatients(this.settings.hospital.hospitalId)
          this.events.publish('billing:done');
        })
    } else {
      let patientInfo = this.patients.splice(0, 1)[0];
      let patientItem = this.patientEntities.splice(0, 1)[0];
      patientInfo.hospital = this.settings.hospital.hospitalDetail.name;
      patientInfo.items = patientItem.billDetails;
      patientInfo.created = moment().format('MM/DD/YYYY');
      this.printPatient(patientInfo);
    }
  }

  printPatient(patient: any) {
    this.printerService.headerCard(this.settings.pdf, this.settings.appUserName)
      .then(() => {
        return this.printerService.patientCard(this.settings.pdf, patient);
      })
      .then(() => {
        if (!_.isEmpty(patient.items)) {
          return this.printerService.recordsTable(this.settings.pdf, patient, this.settings.actionType);
        }
      })
      .then(() => {
        return this.removePatientFromCart(patient.hospitalId, patient.patientId)
      })
      .then(() => {
        if (this.patients.length >= 1) {
          this.settings.pdf.addPage();
        }
        this.addPage();
      })
      .catch((err: any) => {
        this.hideLoading();
      });
  }

  pdfWrite() {
    return new Promise((resolve) => {
      this.writeFile(this.settings.directory, this.settings.fileName, this.settings.pdf)
        .then(() => {
          return this.getMailList(this.settings.hospital.hospitalId)
        })
        .then((mailList: any) => {
          let mailOptions = {
            subject: 'Batch Billing Statement',
            body: 'Find the attached billing report'
          }
          return this.sendMail(mailList, this.platform.is('ios') && this.iosFilePath ? this.iosFilePath : this.settings.directory + this.settings.fileName, mailOptions);
        })
        .then(() => {
          resolve();
        })
    });
  }


  sendAnalytics(startDate: any, endDate: any, items: any) {
    this.showLoading();
    let settings: any, hospitalName: any, hospitalId: any;
    this.storageService.getSettings('HOSPITAL_DATA')
      .then((hospital: any) => {
        hospitalId = hospital.hospitalId;
        if(hospital && hospital.hospitalDetail && hospital.hospitalDetail.name){
          hospitalName = hospital.hospitalDetail.name;
        }
        else if(hospital && hospital.name ){
          hospitalName = hospital.name;
        }
        return this.printerService.openPdf('Analytics-report');
      })
      .then((pdfProperties: any) => {
        this.settings = {
          pdf: pdfProperties.doc,
          fileName: pdfProperties.fileName,
          directory: pdfProperties.directory
        };
        return this.storage.get('name')
      })
      .then((appUserName) => {
        return this.analyticsService.headerCard(this.settings.pdf, `${startDate} - ${endDate}   For   ${hospitalName}`, appUserName);
      })
      .then(() => {
        return this.analyticsService.patientStatus(this.settings.pdf, items);
      })
      .then(() => {
        return this.analyticsService.statsTable(this.settings.pdf, items);
      })
      .then(() => {
        return this.writeFile(this.settings.directory, this.settings.fileName, this.settings.pdf);
      })
      .then(() => {
        return this.getMailList(hospitalId);
      })
      .then((emailList: any) => {
        this.hideLoading();
        let mailOptions = {
          subject: 'Analytics Report',
          body: 'Find the attached Analytics Report'
        }
        this.sendMail(emailList, this.platform.is('ios') && this.iosFilePath ? this.iosFilePath : this.settings.directory + this.settings.fileName, mailOptions);
      })
      .catch((err: any) => {
        this.hideLoading();
        console.log('Error occured ', err);
      });
  }

  basicAlert(name: any) {
    let alert = this.alertCtrl.create({
      title: 'Scan not supported',
      subTitle: 'Please use Manual option. Scan is currently not supported for ' + name + '. Contact support@ptscout.com to enable scanning.',
      buttons: ['Ok']
    });
    alert.present();
  }

  confirmPrompt(title: any, message: any, buttonText: any) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: title,
        message: message,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              reject("NO_ERROR");
            }
          },
          {
            text: buttonText,
            handler: () => {
              resolve();
            }
          }
        ]
      });
      alert.present();
    });
  }

  showMessage(msg: any, duration?: any, cssClass?: string) {
    this.toastCtrl.create({
      message: msg,
      duration: duration || 2000,
      cssClass: cssClass || ''
    }).present();
  }

  setUid(uid: string): Promise<any> {
    return this.storage.set('uid', uid).then(() => {
      return 'sucess';
    })
      .catch(() => {
        return 'failed';
      })
  }

  getUid(): Promise<string> {
    return this.storage.get('uid').then((value) => {
      return value;
    })
  }

  isEmpty(value: any) {
    return !value.toString().length;
  }

  askPin(title: any, subTitle: any, pdfCode?: boolean) {
    let self = this;
    pdfCode = pdfCode ? pdfCode : false
    return new Promise((resolve, reject) => {
      this.promptAlert(title, subTitle, 'Continue')
        .then((result: any) => {
          if (!result || self.isEmpty(result)) {
            this.showMessage("ERROR: Passcode, should not be empty")
            reject();
            return false;
          }
          if (result.length !== 4 && !pdfCode) {
            this.showMessage("ERROR: Passcode, should be 4 digit length")
            reject();
            return false;
          }
          if (result.length < 4 && pdfCode) {
            this.showMessage("ERROR: Passcode, should be 4 digit length or more")
            reject();
            return false;
          }
          resolve(result);
        })
        .catch(() => {
          reject();
        });
    });
  }

  // user logged-in persist data locally
  // sub, nickname, email, token
  login(user: any) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.storage.set('hasLoggedIn', true)
        await this.storage.set('uid', user.uid);
        await this.storage.set('name', user.name)
        await this.storage.set('email', user.email)
        await this.fetchUserCloudSettings();
        let userHospitals: any = await this.getUserHospitals();
        if (userHospitals && userHospitals.length) {
          let hospitalId: any = await this.storage.get('HOSPITAL_ID');
          let hospitalData = _.find(userHospitals, { hospitalId: hospitalId });
          await this.storage.set('HOSPITAL_DATA', hospitalData || {});
          resolve();
        }
        else {
          resolve();
        }
      } catch (err) {
        reject(err);
      }
    })
  };

  fetchUserCloudSettings() {
    return new Promise(async (resolve, reject) => {
      let QueryRef: any;
      let promiseArray: any = [];
      try {
        let res: any = await this.apolloService.fetch(QueryRef, getUserSettings, {});
        res.take(1)
          .subscribe(async (res) => {
            let settings = JSON.parse(JSON.stringify(res.getUserSettings || ""));
            if (settings) {
              delete settings.__typename;
              _.each(_.keys(settings), (eachSetting) => {
                settings[eachSetting] ? promiseArray.push(this.storage.set(eachSetting, settings[eachSetting])) : '';
              })
              Promise.all(promiseArray)
                .then(() => {
                  resolve();
                }).catch((err) => {
                  reject(err);
                })
            } else {
              resolve();
            }
          })
      } catch (err) {
        console.log(err)
        reject(err);
      }
    })
  }

  removePatientFromCart(hospitalId: any, patientId: any) {
    return this.apolloService.mutate({ mutation: UpdatePatient, variables: { hospitalId: hospitalId, patientId: patientId, status: this.settings.actionType } });
  }

  // Get Total cart count of all active hospitals
  getTotalCartCount() {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: getUserHospital, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getUserHospitals'].items)).subscribe(data => {
          if (data && data.length) {
            const hospitals = JSON.parse(JSON.stringify(data));
            let badgeCount = 0;
            _.each(hospitals, function(hospital: any) {
              if (hospital.hospitalId && hospital.isRemoved != 'true' && parseInt(hospital.cartCount)) {
                badgeCount += parseInt(hospital.cartCount);
              }
            })
            resolve(badgeCount);
          } else {
            resolve(0);
          }
        });
    })
  }

  async getUserHospitals() {
    return new Promise((resolve) => {
      this.apollo.watchQuery({ query: getUserHospital, fetchPolicy: 'network-only' }).valueChanges
        .pipe(map(result => result.data['getUserHospitals'].items)).subscribe(data => {
          if (data && data.length) {
            resolve(JSON.parse(JSON.stringify(data)));
          } else {
            resolve([]);
          }
        });
    })
  }

  getTopLookupCodes() {
    this.storage.get("SPECIALIZATION_ID")
      .then(async (specializationId) => {
        if (specializationId) {
          let result: any = await this.patientService.getTopLookupsBySpecialization(this.entitiyQueryRef, specializationId)
          result
            .take(1)
            .subscribe(async (data) => {
              let topCodes = JSON.parse(JSON.stringify((_.values(data || {})[0] || {}).items || []));
              this.patientService.topLookupCodes.next(topCodes);
              this.getFavouriteCodes();
            })
        }
      })
  }

  async getFavouriteCodes() {
    try {
      let favouriteCodes: any = await this.patientService.getFavouriteUserCodes(this.favEntitiesQueryRef);
      favouriteCodes.
        take(1)
        .subscribe((favourites) => {
          favourites = _.clone((_.values(favourites)[0] || {}).items || []);
          this.patientService.userFavouriteCodes.next(favourites);
        })
    } catch (err) {
      console.log(err);
    }
  }

  getInCartPatients(hospitalId: any) {
    let queryRef;
    this.patientService.getInCartPatients(queryRef, hospitalId)
      .then((result: any) => {
        result.take(1)
          .subscribe((res: any) => {
            res = JSON.parse(JSON.stringify(res.queryInCartPatientDetails.items));
            this.updateInCartPatients(hospitalId, res.length)
              .then(() => {
                return this.getTotalCartCount();
              })
              .then((count: number) => {
                if (count) {
                  this.scheduleNotification(1, "Unbilled Patients In Cart", `${count} patient(s) in cart need to be billed`);
                  this.badge.set(count)
                    .catch((err) => console.log(err));
                }
                else {
                  this.cancelLocalNotification(1);
                  this.badge.clear();
                }
                this.events.publish('set-badge', count);
              })
          })
      })
      .catch((err: any) => {
        this.hideLoading();
      });
  }

  updateInCartPatients(hospitalId: any, count: any) {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: updateUserHospitals, variables: { hospitalId: hospitalId, cartCount: count } })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in updateUserHospitals ", err);
          reject();
        }));
    });
  }

  updateBilledEntities(hospitalId: any, entities: any) {
    console.log('upserting entities ', entities);
    return new Promise((resolve, reject) => {
      this.apollo.mutate({ mutation: BulkBillingPatient, variables: { hospitalId: hospitalId, diags: entities } })
        .subscribe(data => {
          resolve();
        }, (err => {
          console.log("Error occured in updateUserHospitals ", err);
          reject();
        }));
    });
  }

  setNotificationTime(time: any) {
    return new Promise((resolve) => {
      this.storageService.setSettings('NOTIFICATION_TIME', time)
        .then(() => {
          cordova.plugins.notification.local.isScheduled(1, function(res: any) {
            if (res) {
              time = time.split(':');
              let hour = time[0], minute = time[1];
              cordova.plugins.notification.local.update({ id: 1, trigger: { every: { hour: +hour, minute: +minute } } }, function(res: any) {
                resolve("SUCCESSFUL");
              });
            }
            else {
              resolve("SUCCESSFUL");
            }
          })
        })
        .catch((err) => {
          console.log(err);
          resolve(null);
        });
    })
  }

  setAllSettings() {
    return new Promise((resolve) => {
      let self = this, promises = [];
      this.storageService.getAllSettings()
        .then((settingsResult: any) => {
          console.log('settingsResult ', settingsResult);
          for (var value in settingsResult) {
            if (settingsResult[value]) {
              promises.push(self.storageService.setLocalSettings(value, settingsResult[value]));
            }
          }
          resolve(Promise.all(promises));
        })
    });
  }

  // Function to remove all hospital related data
  // will remove hospital, mailList, Patient, Notes, records
  purgeHospital(hospitalId: any) {
    return new Promise(async(resolve, reject) => {
      let idToken = (await Auth.currentSession()).getIdToken().getJwtToken();
      let headers: any = this.getHeaders(idToken);
      const options = new RequestOptions({ headers: headers });

      let query = api.purgeData + '?hospitalId=' + hospitalId + '&deleteKey=hospital';
      this.http.delete(query, options)
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            reject(err);
            console.log('Analytics err ', err);
          })
    });
  }
  async getIdToken() {
    return new Promise((resolve, reject) => {
      Auth.currentSession()
        .then((response: any) => {
          resolve(response.getIdToken().getJwtToken())
        })
    })
  }

  getAnalytics(hospitalId: any, from: any, to: any) {
    return new Promise(async (resolve, reject) => {
      let idToken = await this.getIdToken();
      let headers: any = this.getHeaders(idToken);

      const options = new RequestOptions({ headers: headers });

      let query = api.analytics + '?hospitalId=' + hospitalId + '&from=' + from + '&to=' + to;
      this.http.get(query, options)
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            reject(err);
            console.log('Analytics err ', err);
          })
    });
  }

  callDeleteApi(queryParams: string) {
    return new Promise(async (resolve, reject) => {
      let idToken = (await Auth.currentSession()).getIdToken().getJwtToken();
      let headers: any = this.getHeaders(idToken);
      const options = new RequestOptions({ headers: headers });
      let query = api.purgeData + queryParams;
      this.http.delete(query, options)
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            reject(err);
          })
    });
  }

  scanCard(base64data: any, hospitalCode: any) {
    return new Promise(async(resolve, reject) => {
      let idToken = (await Auth.currentSession()).getIdToken().getJwtToken();
      let headers: any = this.getHeaders(idToken),
        options = new RequestOptions({ headers: headers }),
        body: any = JSON.stringify({ body: { base64data: base64data, hospitalCode: hospitalCode } });
      this.http.post('https://l9epj2t9cb.execute-api.us-east-1.amazonaws.com/Prod/imageToCard', body, options)
        .subscribe((data: any) => {
          console.log(data);
          resolve(JSON.parse(JSON.parse(data._body).body));
        });
    })
  }

  getHeaders(jwtToken: any) {
    let headers = new Headers();
    headers.append('Authorization', jwtToken);
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  setLocalNotification(id: number, title: string, text: string) {
    if (!this.platform.is('core')) {
      this.scheduleNotification(id, title, text);
    }
  }

  cancelLocalNotification(id: number) {
    if (!this.platform.is('core')) {
      cordova.plugins.notification.local.cancel(id, function(res: any) {
      });
    }
  }

  scheduleNotification(id: number, title: string, text: string) {
    return new Promise((resolve, reject) => {
      this.storageService.getSettings('NOTIFICATION_TIME')
        .then((time: any) => {
          console.log(time);
          time = (time || '17:00').split(':');
          let hour = time[0], minute = time[1];
          let scheduleObj = {
            id: id,
            title: title,
            text: text,
            trigger: { every: { hour: +hour, minute: +minute } },
          };
          cordova.plugins.notification.local.isScheduled(id, function(res: any) {
            if (res) {
              cordova.plugins.notification.local.cancelAll(function() {
                cordova.plugins.notification.local.schedule(scheduleObj);
                resolve();
              });
            }
            else {
              cordova.plugins.notification.local.schedule(scheduleObj);
              resolve();
            }
          })
        })
        .catch((err: any) => {
          console.log(err);
        })
    })
  }
}
