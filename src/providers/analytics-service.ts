import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import * as _ from 'underscore';

let jsPDF = require('jspdf');
require('jspdf-autotable');

@Injectable()

export class AnalyticsService {

  constructor(
    public platform: Platform,
    public file: File
  ) { }

  openPdf(name: any) {
    return new Promise((resolve) => {
      let doc = new jsPDF('p', 'pt', 'letter'),
        d = new Date(),
        directory = (this.platform.is('ios') ? this.file.dataDirectory : this.file.externalRootDirectory),
        fileName = name + '-' + ((d.getMonth() + 1) + '-' + d.getDate() + '-' + d.getFullYear()) + '_' + d.getHours() + d.getMinutes() + d.getSeconds() + '.pdf';

      resolve({
        fileName: fileName,
        doc: doc,
        directory: directory
      });
    })
  };

  headerCard(pdf: any, duration: any, username:any) {
    pdf.autoTable(
      [{
        title: 'Analytics',
        dataKey: 'date'
      },], [{
        date: (username || '')
      },
        {
          date: (duration || '')
        }], {
        startY: 40,
        theme: 'grid',
        styles: {
          cellPadding: 5
        },
        headerStyles: {
          rowHeight: 25,
          fontSize: 12,
          valign: 'middle',
          halign: 'right',
          fontStyle: 'bold'
        },
        bodyStyles: {
          rowHeight: 25,
          fontSize: 11,
          valign: 'middle',
          halign: 'right',
          fillColor: 255,
          textColor: 0
        }
      });
  };

  patientStatus = function(pdf: any, items: any) {
    var columns = [{
      title: 'Patients',
      dataKey: 'status'
    },{
      title: 'Count',
      dataKey: 'count'
    },];

    pdf.setTextColor(0);
    pdf.autoTable(columns, [{ status: 'TOTAL', count: items.totalPatients }, { status: 'ACTIVE', count: items.totalActive }, { status: 'BILLED', count: items.totalInArchieve }, { status: 'CART', count: items.totalInCart }], {
      theme: 'grid',
      startY: 120,
      styles: {
        cellPadding: 5
      },
      headerStyles: {
        rowHeight: 20,
        fontSize: 10
      },
      bodyStyles: {
        rowHeight: 15,
        fontSize: 10,
        valign: 'middle'
      },
      columnStyles: {
        count: {
          columnWidth: 100
        }
      },
      alternateRow: {
        fillColor: 245
      }
    });
  };

  statsTable(pdf: any, records: any) {
    console.log('am in statsTable ', records);
    let allRecords = records;
    let startY = 200;
    let categories = {
      billed_diagnosis: 'Billed Diagnoses',
      billed_em_code: 'Billed E/M Codes',
      billed_procedure: 'Billed Procedures',
      unbilled_diagnosis: 'Active Diagnoses',
      unbilled_em_code: 'Active E/M Codes',
      unbilled_procedure: 'Active Procedures'
    };
    let columns = [{
      title: 'Code',
      dataKey: 'code'
    }, {
      title: 'Name',
      dataKey: 'name'
    },
     {
      title: 'Count',
      dataKey: 'count'
    },];

    pdf.setFontSize(11);
    pdf.setFontSize(13);
    pdf.setFontStyle('bold');
    pdf.setTextColor(0, 0, 0);
    pdf.setFontSize(11);
    pdf.setFontType('normal');

    let temp = [];
    Object.keys(allRecords).forEach(function(item:any) {
      if(_.isObject(allRecords[item]) && Object.keys(allRecords[item]).length) {
        temp = [];
        Object.keys(allRecords[item]).forEach(function(value:any) {
          temp.push({
            code: (value.split(',')[0]).replace(/[^\w\s]/gi, ''),
            name: (value.split(',')[1]).replace(/[^\w\s]/gi, ''),
            count: allRecords[item][value]
          })
        })
          pdf.setTextColor(0);
          startY = startY + 65,
          pdf.text(categories[item], 100, startY);
          pdf.autoTable(columns,temp, {
            theme: 'grid',
            startY: startY + 10,
            styles: {
              cellPadding: 5
            },
            headerStyles: {
              fontSize: 10
            },
            bodyStyles: {
              fontSize: 10,
              valign: 'middle'
            },
            columnStyles: {
              code: {
                columnWidth: 75
              },
              count: {
                columnWidth: 100
              }
            },
            alternateRow: {
              fillColor: 245
            }
          });
        startY = startY + (Object.keys(allRecords[item]).length * 15) + 20;
      pdf.setFontSize(11);
    }
    })
  };

}
