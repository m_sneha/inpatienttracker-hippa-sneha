export default{
  Auth: {
    mandatorySignIn: true,
    region: 'us-east-1',
    'aws_appsync_authenticationType': 'AMAZON_COGNITO_USER_POOLS',
    userPoolId: 'us-east-1_ASkPsomTA',
    // identityPoolId: 'us-east-1:47d78e00-3cb1-4118-a253-4c0d2bfd6308',
    userPoolWebClientId: '5t2hteso0tgltd0pmkha1gelda',
    // clientId: '5t2hteso0tgltd0pmkha1gelda',
    'aws_user_pools': 'enable',
    'aws_user_pools_mfa_type': 'OFF',
  },
  API: {
    endpoints: [
      {
        name: "notes",
        endpoint: 'https://hfsiowz7wbc47pkgwoqbyysrhe.appsync-api.us-east-1.amazonaws.com/graphql',
        region: 'us-east-1'
      },
    ]
  }
}
