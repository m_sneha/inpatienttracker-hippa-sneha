import { Component, NgZone, ViewChild } from '@angular/core';
import { Events, IonicApp, MenuController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Rx';

import { Nav } from 'ionic-angular';
import { WelcomePage } from '../pages/welcome/welcome';
import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { CartList } from '../pages/cart/cart-list/cart-list';
import { SwitchHospital } from '../pages/switch-hospital/switch-hospital';
import { AnalyticsPage } from "../pages/analytics/analytics";
import { LoginComponent } from "../pages/authentication/login/login";
import { ArchivePage } from "../pages/archives/archives";
import { SettingsPage } from '../pages/settings/settings';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { Badge } from '@ionic-native/badge';
import { LockScreenComponent } from "../components/lock-screen/lock-screen";
import { GraphqlService } from './graphql.service'
import { Helper } from '../providers/helper';
import { SpecializationService } from '../providers/specialization-service';
import { HospitalService } from '../providers/hospitalService';
import * as AWS from "aws-sdk/global";
import moment from 'moment';
import { ManageSubscriptions, SubscriptionHelperSerivce, SubscriptionListing } from '../subscriptions-module';
import { Auth } from 'aws-amplify';
import { Apollo } from 'apollo-angular';

declare const cordova: any;

export interface PageInterface {
  title: string;
  component: any;
  icon: string;
  hideFor?: number;
  logsOut?: boolean;
  index?: number;
  tabComponent?: any;
  notifications?: any;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  rootPage: any;
  menuItems: any = [];
  isCordovaEnabled: boolean;
  lastTimeBackPress = 0;
  timePeriodToExit: number = 3000;
  showMenu: boolean = true;
  networkInfo: any = {
    isLive: true,
    message: '',
    class: ''
  }
  user: any = null;
  sessionRefreshSubscription: any;
  hasLoggedIn: any;
  userAppReadySubscription: any;
  testEnvironment:boolean;
  @ViewChild(Nav) nav: Nav;

  loggedInPages: PageInterface[] = [
    { title: 'Home', component: HomePage, icon: 'ios-home-outline' },
    { title: 'Billing', component: CartList, icon: 'ios-filing-outline' },
    { title: 'Account', component: AccountPage, icon: 'ios-person-outline' },
    { title: 'Manage Subscriptions', component: ManageSubscriptions, icon: 'cart' },
    { title: 'Switch Hospital', component: SwitchHospital, icon: 'ios-pin-outline' },
    { title: 'Settings', component: SettingsPage, icon: 'ios-settings-outline' },
    // { title: 'Welcome', component: WelcomePage, icon: 'ios-book-outline' },
    // { title: 'Settings', component: SettingsPage, icon: 'ios-settings-outline' },
    { title: 'Analytics', component: AnalyticsPage, icon: 'ios-stats-outline' },
    // { title: 'Report a Problem', component: 'report_issue', icon: 'ios-megaphone-outline' },
    { title: 'Archive', component: ArchivePage, icon: 'ios-list-box-outline' },
    { title: 'Lock', component: 'lock_screen', icon: 'ios-lock-outline' },
    { title: 'Logout', component: 'log_out', icon: 'log-out', logsOut: true }
  ];

  loggedOutPages: PageInterface[] = [
    { title: 'Login', component: LoginComponent, icon: 'log-in' }
  ];

  adminPages: PageInterface[] = [
    { title: 'Posts', component: 'PostsPage', icon: 'ios-paper-outline' }
  ];

  constructor(
    public apollo: Apollo,
    public device: Device,
    public events: Events,
    public graphqlService: GraphqlService,
    public helper: Helper,
    public hospitalService: HospitalService,
    public ionicApp: IonicApp,
    public menu: MenuController,
    public network: Network,
    private _ngZone: NgZone,
    public platform: Platform,
    private badge: Badge,
    public splashScreen: SplashScreen,
    public specializationService: SpecializationService,
    public statusBar: StatusBar,
    public storage: Storage,
    public subscriptionHelper: SubscriptionHelperSerivce
  ) {
    let self: any = this;
    this.isCordovaEnabled = (this.device.cordova !== null);
    document.addEventListener('deviceready', function() {
      localStorage.setItem('canGotoCart', (cordova.plugins.notification.local.launchDetails || {}).id);
    }, false);

    let pausedEvent: any;
    this.platform.pause.subscribe((_pausedEvent: any) => {
      pausedEvent = _pausedEvent;
      console.log('inside the platform _pausedEvent', _pausedEvent)
    });
    if (document.URL.indexOf("?") > 0) {
      let splitURL = document.URL.split("?");
      let splitParams = splitURL[1].split("&");
      let i: any;
      for (i in splitParams) {
        let singleURLParam = splitParams[i].split('=');
        if (singleURLParam[0] == "environment" && singleURLParam[1] == 'QA-Test') {
          this.testEnvironment  = true;
          localStorage.setItem('TEST_ENVIRONMENT', 'true');
        }
      }
    }
    else{
      localStorage.removeItem('TEST_ENVIRONMENT');
    }
    this.storage.ready()
      .then(() => {
        this.platform.resume.subscribe((_resumedEvent: any) => {
          console.log('inside the platform _resumedEvent', _resumedEvent)
          if (pausedEvent && pausedEvent.timeStamp && !self.testEnvironment) {
            this.storage.get('AUTO_LOCK_IN_MINUTES')
              .then((minutes: any) => {
                if (+minutes && (_resumedEvent.timeStamp - pausedEvent.timeStamp) / 1000 > (minutes * 60)) {
                  this.showLockScreen();
                }
              });
          }
          this.listenToLoginEvents();
        });
      });

    this.badge.hasPermission()
      .then((res) => {
        if (!res) {
          this.badge.requestPermission();
        }
      })
      .catch((err: any) => {
        console.log(err);
      })

    this.network.onDisconnect().subscribe(() => {
      this._ngZone.run(() => {
        this.networkInfo.message = 'No connection';
        this.networkInfo.class = 'network disconnected';
      })
    });

    this.network.onConnect().subscribe(() => {
      this._ngZone.run(() => {
        this.networkInfo.message = 'Back online';
        this.networkInfo.class = 'network connected';
        setTimeout(() => {
          this.networkInfo.message = '';
        }, 5000);
      });
    });

    this.storage.ready()
      .then(() => {
        return new Promise((resolve, reject) => {
          self.statusBar.backgroundColorByHexString('#2C4099');
          self.storage.get('hasLoggedIn')
            .then((hasLoggedIn: any) => {
              self.hasLoggedIn = hasLoggedIn;
              if (hasLoggedIn) {
                self.showMenu = true;
                self.menuItems = self.loggedInPages;
                self.rootPage = HomePage;
                Auth.currentUserPoolUser()
                  .then((currentUserPoolUser: any) => {
                    return graphqlService.hydrateClient();
                  })
                  .then(() => {
                    resolve();
                  })
                  .catch((err) => {
                    console.log('err', err);
                    self.showMenu = false;
                    self.menuItems = self.loggedOutPages;
                    self.rootPage = LoginComponent;
                  })
              }
              else {
                self.menuItems = self.loggedOutPages;
                self.showMenu = false;
                self.rootPage = LoginComponent;
                resolve();
              }
            });
        })
      })
      .then(() => {
        self.platformReady()
          .then(() => {
            if (self.hasLoggedIn) {
              self.helper.getTopLookupCodes();
              self.specializationService.findAll();
              self.hospitalService.initApollo();
              self.splashScreen.hide();
              self.storage.get('STRIPE_CUST_ID')
                .then((stripeCustId: any) => {
                  if ((stripeCustId && stripeCustId != 'null') || self.testEnvironment) {
                    self.subscriptionHelper.isSubscriptionActive(stripeCustId)
                      .then((response: any) => {
                        if (response.isExists || self.testEnvironment) {
                          this.splashScreen.hide();
                          self.nav.setRoot(self.rootPage);
                          if(!self.testEnvironment){
                            self.showLockScreen();
                          }
                        }
                        else {
                          this.splashScreen.hide();
                          self.nav.setRoot(SubscriptionListing);
                        }
                      });
                  }
                  else {
                    this.splashScreen.hide();
                    self.nav.setRoot(WelcomePage);
                    self.showLockScreen();
                  }
                })
            }
            else {
              this.splashScreen.hide();
              self.nav.setRoot(self.rootPage);
            }
          })
      })
      .catch((err) => {
        console.log('err occured' + err);
      });
    this.listenToLoginEvents();
  }
  // constructor end

  listenToLoginEvents() {
    this.userAppReadySubscription = this.events.subscribe(('user:app-ready'), () => {
      if (this.userAppReadySubscription) {
        this.userAppReadySubscription.unsubscribe();
      }
      this.helper.showMessage('Welcome to Inpatient Tracker');
      this.events.publish('user:login');
      this.helper.storageService.setSettings('APP_READY', "true")
        .then(() => {
          this.menu.swipeEnable(true);
          this.menu.enable(true);
          this.nav.setRoot(HomePage);
        })
    })

    let self: any = this;
    this.events.subscribe('user:login', () => {
      this.showMenu = true;
      this.menuItems = this.loggedInPages;
      this.helper.getTopLookupCodes();
      self.specializationService.findAll();
      self.hospitalService.initApollo();
    });

    this.events.subscribe('user:logout', () => {
      this.showMenu = false;
      localStorage.removeItem('TEST_ENVIRONMENT');
      this.menuItems = this.loggedOutPages;
      // this.hospitalService.inValidateSubscriptions();
      if (this.sessionRefreshSubscription) {
        this.sessionRefreshSubscription.unsubscribe();
      }
    });
  }

  platformReady() {
    return new Promise((resolve, reject) => {
      this.platform.ready()
        .then(() => {
          if (this.isCordovaEnabled && (this.platform.is('android') || this.platform.is('ios'))) {
            this.platform.registerBackButtonAction(() => {
              let activePortal = this.ionicApp._loadingPortal.getActive() ||
                this.ionicApp._modalPortal.getActive() ||
                this.ionicApp._toastPortal.getActive() ||
                this.ionicApp._overlayPortal.getActive();

              if (activePortal) {
                activePortal.dismiss();
                return;
              }
              if (this.menu.isOpen()) {

                this.menu.close();
                return;
              }
              if (this.nav.getActive() && (((this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-HOME') && (this.nav.getActive().pageRef().nativeElement.tagName !== 'SUBSCRIPTION-LISTING')) && this.nav.canGoBack())) {
                this.nav.pop();
                return;
              }

              if (this.nav.getActive() && (((this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-HOME') &&
                (this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-WELCOME') &&
                (this.nav.getActive().pageRef().nativeElement.tagName !== 'SUBSCRIPTION-LISTING') &&
                (this.nav.getActive().pageRef().nativeElement.tagName !== 'LOCK-SCREEN') &&
                (this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-USER')) && !this.nav.canGoBack())) {
                this.storage.get('hasLoggedIn')
                  .then((hasLoggedIn) => {
                    if (hasLoggedIn) {
                      this.showMenu = true;
                      this.rootPage = HomePage;
                      this.nav.setRoot(HomePage);
                      // this.showLockScreen();
                    } else {
                      this.showMenu = false;
                      this.rootPage = LoginComponent;
                    }
                  });
                return;
              }

              if (this.nav.getActive() && this.nav.getActive().pageRef().nativeElement.tagName == 'PAGE-HOME' ||
                (this.nav.getActive().pageRef().nativeElement.tagName == 'PAGE-WELCOME' ||
                  this.nav.getActive().pageRef().nativeElement.tagName == 'LOCK-SCREEN' ||
                  this.nav.getActive().pageRef().nativeElement.tagName == 'SUBSCRIPTION-LISTING'
                  || this.nav.getActive().pageRef().nativeElement.tagName == 'PAGE-USER')) {
                if ((+new Date()) - this.lastTimeBackPress < this.timePeriodToExit) {
                  this.platform.exitApp();
                }
                else {
                  this.helper.showMessage('Press back again to exit App', 3000);
                  this.lastTimeBackPress = +(new Date());
                }
              }
            })
          }
          this.splashScreen.hide();
          resolve();
        });
    });
  }

  showLockScreen(param?:string) {
    let passcode: any, touchId: any, appReady: any;

    if (this.rootPage.name == 'LoginComponent' && !param) {
      this.nav.setRoot(LoginComponent);
      return false;
    }

    this.storage.get('PASSCODE')
      .then((result: any) => {
        passcode = result;
        return this.storage.get('APP_READY')
      })
      .then((result: any) => {
        appReady = result;
        return this.storage.get('TOUCH_ID')
      })
      .then((result) => {
        touchId = result;

        if (appReady != 'true') {
          this.rootPage = WelcomePage;
          this.menuItems = this.loggedOutPages;
        }

        if (passcode) {
          this.menuItems = this.loggedInPages;
          this.nav.setRoot(LockScreenComponent, {
            code: passcode || '0000',
            ACDelbuttons: true,
            component: this.rootPage,
            touchId: touchId ? true : false,
            onCorrect: function() {
              console.log('Correct!');
            },
            onWrong: function(attemptNumber: any) {
              console.log(attemptNumber + ' wrong passcode attempt(s)');
            }
          });
        } else {
          this.menuItems = this.loggedOutPages;
          this.nav.setRoot(WelcomePage);
        }
      })
      .catch((err: any) => {
        console.log(err);
      });
  }

  enableMenu(loggedIn: boolean) {
    if (loggedIn) {
      this.menuItems = this.loggedInPages;
    } else {
      this.menuItems = this.loggedOutPages;
    }
  }

  openPage(page: PageInterface) {
    // the nav component was found using @ViewChild(Nav)
    // reset the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    if (page.component == 'log_out') {
      this.helper.showLoading();
      Auth.signOut()
        .then(() => {
          return this.storage.clear();
        })
        .then(()=>{
          return this.apollo.getClient().cache.reset();
        })
        // .then(()=>{
        //   return this.apollo.getClient().resetStore();
        // })
        .then(()=>{
          this.helper.hideLoading();
          this.helper.showMessage('User logged-out successfully')
          this.nav.setRoot(LoginComponent);
        })
        .catch((err) => {
          this.helper.hideLoading();
          console.log('faield to log-out the user', err);
          this.helper.showMessage('Failed to log-out the current user');
        })
    } else if (page.component == 'lock_screen') {
      this.showLockScreen('lock');
      return false;
    }
    else if (page.index) {
      this.nav.setRoot(page.component, { tabIndex: page.index });
    } else {
      this.nav.setRoot(page.component);
    }
  }

}
