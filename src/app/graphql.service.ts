import { Injectable } from '@angular/core';
import appSyncConfig from './aws-export';
import { Apollo } from 'apollo-angular';
import { ApolloLink, concat } from 'apollo-link';
import { HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import AWSAppSyncClient from 'aws-appsync';
import { ApolloClient } from 'apollo-client';
import { Auth } from 'aws-amplify';

@Injectable()
export class GraphqlService {

  constructor(
    public apollo: Apollo,
    public httpLink: HttpLink
  ) {
  }

   hydrateClient() {
    return new Promise(async (resolve, reject) => {
      if (!this.apollo.getClient()) {
        // let idToken = (await Auth.currentSession()).getIdToken().getJwtToken();
        // const http = this.httpLink.create({ uri: appSyncConfig.API.endpoints[0].endpoint });
        // const authMiddleware = new ApolloLink((operation, forward) => {
        //   // add the authorization to the headers
        //   operation.setContext({
        //     headers: {
        //       Authorization: idToken || ''
        //     }
        //   });
        //   return forward(operation);
        // });
        // let apolloClient = this.apollo.create({
        //   link: concat(authMiddleware, http),
        //   cache: new InMemoryCache()
        // });
        // resolve();
        //offline cache
        // this.apollo.setClient(apolloClient);
        // apolloClient.hydrated()
        //  .then(() => {
        //    resolve();
        //  })
        const appsyncClient = new AWSAppSyncClient({
          url: appSyncConfig.API.endpoints[0].endpoint,
          region: appSyncConfig.Auth.region,
          auth: {
            type: appSyncConfig.Auth.aws_appsync_authenticationType,
            jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken()
          },
          disableOffline: true
        });
        const client = new ApolloClient(appsyncClient);
        this.apollo.setClient(appsyncClient);
        appsyncClient.hydrated()
          .then(() => {
            resolve();
          })
      }
      else {
        resolve();
      }
    })
  }
}
