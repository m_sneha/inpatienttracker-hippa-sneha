import gql from 'graphql-tag';

export const CreateNotes = gql`
mutation userNotes($patientId: String!, $date: String, $title: String, $description: String) {
  createUserNotes(input: {
    patientId: $patientId,
    date: $date,
    title: $title,
    description: $description
  }) {
      id
      date
      description
      title
  }
}`;

export const GetNotes = gql`
query ($patientId:String!){
  getUserNotesForPatient(patientId:$patientId){
    items{
      id
      date
      title
      description
    }
  }
}`;

export const UpdateNotes = gql`
mutation ($id:String!,$patientId: String!, $date: String, $title: String, $description: String){
  updateUserNotes(input:{
    patientId: $patientId,
    date: $date,
    title: $title,
    description: $description,
    id: $id
  }){
      id
      date
      title
      description
  }
}`;

export const DeleteNotes = gql`
mutation ($patientId:String!, $id:String!){
  deleteUserNotes(input:{
    patientId:$patientId,
    id:$id
  }){ id }
}`;

export const getTopLookups = gql`
  query($specializationId:String!){
    getTopLookupCodesForSpecialization(specializationId:$specializationId){
      items{
        code
        name
        codeType
      }
    }
  }`;

export const getPatientEntities = gql`
    query ($hospitalId:String!, $patientId:String!){
      getDiagnosisAndBillingForPatient(patientId:$patientId, hospitalId:$hospitalId)
      {
        items {
          patientId
          billStatus
          createdOn
          billDetails{
            em_code{
              name
              code
              isBilled
              date
            }
            diagnosis{
              name
              code
              isBilled
              date
            }
            procedures{
              name
              code
              isBilled
              date
            }
          }
        }
      }
    }`;

export const addNewEntities = gql`
  mutation addNewEntities(
      $patientId:String!,
      $billStatus:String,
      $createdOn:String!,
      $hospitalId:String!,
      $billDetails:BillDetailsInput){
      createDiagnosisAndBilling(input:{
        patientId: $patientId
        billStatus: $billStatus
        createdOn: $createdOn
        billDetails: $billDetails
        hospitalId: $hospitalId
      }){
        patientId
        billStatus
        createdOn
        billDetails{
        	em_code{
            name
            code
          }
          diagnosis{
            name
            code
          }
          procedures{
            name
            code
          }
      	}
      }
    }`;

export const updateEntities = gql`
  mutation addNewEntities(
      $patientId:String!,
      $updatedOn:String!,
      $hospitalId:String!,
      $billStatus:String,
      $billDetails:BillDetailsInput){
      updateDiagnosisAndBilling(input:{
        patientId: $patientId
        billStatus: $billStatus
        updatedOn: $updatedOn
        hospitalId: $hospitalId
        billDetails: $billDetails
      }){
        patientId
        billStatus
        createdOn
      }
    }`;

export const deleteAllEntities = gql`
  mutation deleteEntities($hospitalId:String!, $patientId:String!) {
    deleteDiagnosisAndBilling(input:{
      hospitalId:$hospitalId
      patientId:$patientId
    }){
      patientId
    }
  }`;

export const createFavouriteEntities = gql`
  mutation createUserFavoriteTables($code: String!, $codeType: String, $isFav: String, $name: String){
    createUserFavoriteTables(input:{
      code : $code
      name : $name
      isFav : $isFav
      codeType : $codeType
    }){
      code
      name
      isFav
      codeType
    }
  }`;

export const updateFavouriteEntities = gql`
  mutation updateUserFavoriteTables($code: String!, $codeType: String, $isFav: String, $name: String){
    updateUserFavoriteTables(input:{
      code : $code
      name : $name
      isFav : $isFav
      codeType : $codeType
    }){
      code
      name
      isFav
      codeType
    }
  }`;

export const deleteFavouriteEntities = gql`
  mutation deleteUserFavoriteTables($code: String!){
    deleteUserFavoriteTables(input:{
      code : $code
    }){
      code
    }
  }`;

export const getUserFavoriteCodes = gql`
  query getUserFavoriteCodes{
    getUserFavoriteCodes{
      items{
       code
       codeType
       isFav
       name
      }
    }
  }`;

export const updateMultipleFavEntities = gql`
    mutation createMultipleFavoriteTables($favs:[CreateUserFavoriteTablesInput!]){
    createMultipleFavoriteTables(userfavs:$favs){
        code
        name
    }
  }`;

export const searchIcdCodes = gql`
  query getIcds($from:Int, $search:String){
    getIcds(from:$from,search:$search){
      code
      name
    }
  }`;

export const searchCptCodes = gql`
  query getCpts($from:Int, $search:String){
    getCpts(from:$from,search:$search){
      code
      name
    }
  }`;
