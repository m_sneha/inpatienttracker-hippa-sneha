import gql from 'graphql-tag';

export const GetActivePatient = gql`
query listPatientDetails($hospitalId: String!){
  queryActivePatientDetails(hospitalId:$hospitalId){
    items{
      patientId
    	created
    	dateOfAdmission
    	dateOfBirth
    	dischargeDate
    	followUpInWeeks
    	hospitalId
    	medicalRecordNumber
    	name
    	referringDoctor
    	room
    	sex
    	status
      accountNumber
    	lastUpdated
    }
  }
}
`;

export const GetCartPatientEntities = gql`
query getMultiplePatientDiagnosis(
  $hospitalId: String!,
  $patientIds: [String]!
  ){
    getMultiplePatientDiagnosis(mulPats: { hospitalId: $hospitalId, patientIds: $patientIds}){
      patientId
      createdOn
     billDetails{
       em_code{
         code
         name
         date
         isBilled
       }
       diagnosis{
         code
         name
         date
         isBilled
       }
       procedures {
         code
         name
         date
         isBilled
       }
     }
  }
}
`;

export const GetCartPatient = gql`
query queryInCartPatientDetails($hospitalId: String!){
  queryInCartPatientDetails(hospitalId:$hospitalId){
    items{
      patientId
    	created
    	dateOfAdmission
    	dateOfBirth
    	dischargeDate
    	followUpInWeeks
    	hospitalId
    	medicalRecordNumber
    	name
    	referringDoctor
    	room
    	sex
    	status
    	lastUpdated
      accountNumber
    }
  }
}
`;

export const MarkVisitedPatient = gql`
mutation UpdateVisitedPatient($patientId:String!, $hospitalId: String!,  $lastUpdated: String!){
  updatePatientDetail(input:{
    patientId:$patientId,
    hospitalId:$hospitalId,
    lastUpdated:$lastUpdated,
  }){
    name
    patientId
    created
    dateOfAdmission
    dateOfBirth
    medicalRecordNumber
    room
    sex
    status
    lastUpdated
    accountNumber
    hospitalId
  }
}`

export const BulkBillingPatient = gql`
mutation updateMultipleDiagnosis($hospitalId:String!, $diags: [UpdateMultiDiagnosisAndBillingInput!]){
  updateMultipleDiagnosisTables(hospitalId:$hospitalId
  diags:$diags
 ){
    patientId
  }
}`

export const AddNewPatient = gql`
  mutation createPatientDetail($dateOfAdmission:String, $dateOfBirth:String, $hospitalId:String!, $medicalRecordNumber:String, $accountNumber:String, $name:String, $referringDoctor:String, $room:String, $sex:String){
    createPatientDetail(input:{
      dateOfAdmission:$dateOfAdmission
      dateOfBirth:$dateOfBirth
      hospitalId:$hospitalId
      medicalRecordNumber:$medicalRecordNumber
      name:$name
      referringDoctor:$referringDoctor
      room:$room
      sex:$sex
      status:"ACTIVE"
      accountNumber:$accountNumber
    }){
      patientId
    }
  }`;

export const UpdatePatient = gql`
mutation updatePatientDetail(
  $hospitalId:String!,
  $patientId:String!,
  $dateOfAdmission:String,
  $dateOfBirth:String,
  $patientId:String!,
  $medicalRecordNumber:String,
  $name:String,
  $referringDoctor:String,
  $room:String,
  $sex:String,
  $lastUpdated:String,
  $followUpInWeeks:String,
  $dischargeDate:String
  $accountNumber:String
  $status:String
){
  updatePatientDetail(input:{
    patientId:$patientId
    dateOfAdmission:$dateOfAdmission
    dateOfBirth:$dateOfBirth
    hospitalId:$hospitalId
    medicalRecordNumber:$medicalRecordNumber
    name:$name
    referringDoctor:$referringDoctor
    room:$room
    sex:$sex
    lastUpdated:$lastUpdated
    followUpInWeeks:$followUpInWeeks
    dischargeDate:$dischargeDate
    status:$status
    accountNumber:$accountNumber
  }){
    patientId
    status
  }
}`;

export const DeletePatient = gql`
mutation deletePatientDetail($patientId:String!, $hospitalId:String!){
  deletePatientDetail(input:{
    patientId:$patientId
    hospitalId:$hospitalId
  }){
    patientId
  }
}`;
