import gql from 'graphql-tag';

export const hospitals = gql`
mutation hospitals(
  $userId: String,
  $hospitalId: String,
  $name: String,
  $code: String,
  $shortname: String,
  $address1: String,
  $address2: String,
  $city: String,
  $state: String,
  $zipcode: String
) {
  createUserHospital(input: {
     userId: $userId,
     hospitalId: $hospitalId,
     name: $name,
     code: $code,
     shortname: $shortname,
     address1: $address1,
     address2: $address2,
     city: $city,
     state: $state,
     zipcode: $zipcode
   }) {
     userId
   }
}
`;
