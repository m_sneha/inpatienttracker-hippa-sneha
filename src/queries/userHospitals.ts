import gql from 'graphql-tag';

export const getUserHospital = gql`
query GetUserHospital {
    getUserHospitals {
      items {
        hospitalId
        cartCount
        isRemoved
        isCustom
        hospitalDetail {
          name
          code
          shortname
          address1
          address2
          city
          state
          zipcode
        }
      }
    }
  }
`;

export const getMailingInfo = gql`
query getMailingInfo($hospitalId: String!){
  getMailingInfo(hospitalId:$hospitalId){
    to
    cc
    bcc
  }
}
`;

export const updateMailingInfo = gql`
mutation updateMailingInfo(
  $hospitalId: String!
  $to: String
  $cc: String
  $bcc: String
){
  updateMailingInfo(input: {
    hospitalId:$hospitalId
    to: $to
    cc: $cc
    bcc: $bcc
  }){
    to
    cc
    bcc
  }
}
`;

export const deleteUserHospitals = gql`
mutation deleteUserHospitals($hospitalId: String!) {
  deleteUserHospitals(input: {
    hospitalId: $hospitalId
  }) {
    hospitalId
  }
}
`;

export const deleteMailingInfo = gql`
mutation deleteMailingInfo($hospitalId: String!) {
  deleteMailingInfo(input: {
    hospitalId: $hospitalId
  }) {
    hospitalId
  }
}
`;

export const subscriptionToUserHospitals = gql`
subscription {
  onCreateUserHospitals {
    hospitalId
    isCustom
    isRemoved
    hospitalDetail {
      name
      code
      shortname
      address1
      address2
      city
      state
      zipcode
    }
  }
}
`;

export const createUserHospitals = gql`
mutation createUserHospitals(
  $hospitalId: String!,
  $name: String!,
  $code: String,
  $shortname: String,
  $address1: String,
  $address2: String,
  $city: String,
  $state: String,
  $zipcode: String,
  $isCustom: String
) {
  createUserHospitals(input: {
     hospitalId: $hospitalId,
     isCustom: $isCustom,
     hospitalDetail: {
       name: $name,
       code: $code,
       shortname: $shortname,
       address1: $address1,
       address2: $address2,
       city: $city,
       state: $state,
       zipcode: $zipcode
     }
   }) {
     hospitalId
   }
}`;

export const updateUserHospitals = gql`
mutation updateUserHospitals(
  $hospitalId: String!,
  $cartCount:String
  $isCustom: String
  $isRemoved: String
) {
  updateUserHospitals(input: {
     hospitalId: $hospitalId,
     cartCount: $cartCount
     isCustom: $isCustom
     isRemoved: $isRemoved
   }) {
     hospitalId
   }
}`;

export const createMailingInfo = gql`
mutation createMailingInfo(
  $hospitalId: String!,
  $to: String,
  $cc: String,
  $bcc: String,
) {
  createMailingInfo(input: {
     hospitalId: $hospitalId,
     to: $to,
     cc:$cc,
     bcc:$bcc
   }) {
     hospitalId
   }
}`;
