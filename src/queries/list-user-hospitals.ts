import gql from 'graphql-tag';

export const getUserHospital = gql`
query GetUserHospital($userId: String!) {
    getUserSettings(userId: $userId) {
      userId
      hospitalList {
        hospitalId
        name
        shortname
        address1
      }
    }
  }
`;
