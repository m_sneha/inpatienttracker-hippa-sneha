import { Component, Output, EventEmitter } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';
import { HospitalList } from '../../pages/settings/hospital/hospital-list/hospital-list';
import { AddHospital } from '../../pages/settings/hospital/add-hospital/add-hospital';

import { HospitalService } from '../../providers/hospitalService';
import { Helper } from '../../providers/helper';
import * as _ from 'underscore';

@Component({
  selector: 'hospital-card',
  templateUrl: 'hospital-card.html'
})
export class HospitalCard {
  @Output() updateOptions = new EventEmitter();

  hospitals: any[] = [];
  hospitalLength: any = 0;

  constructor(
    public helper: Helper,
    public events: Events,
    public modalCtrl: ModalController,
    public hospitalService: HospitalService
  ) {
    this.init();
    this.events.subscribe('get:hospitals', () => {
      this.init();
    });
  }

  public init() {
    this.hospitalService.getUserHospitals()
      .then((data: any) => {
        this.hospitals = data;
        this.sendOptions();
      })
  }

  getHospitalLength() {
    let self = this;
    this.hospitalLength = 0;
    _.each(this.hospitals, function(hospital:any){
      if(hospital.hospitalId && hospital.isRemoved != 'true') {
        self.hospitalLength++;
      }
    })
  }

  // list all hospitals
  public chooseHospital() {
    let modal = this.modalCtrl.create(HospitalList);
    modal.present();

    modal.onDidDismiss(() => {
      this.helper.storageService.getSettings('HOSPITAL_ID')
        .then((hospitalId: any) => {
          if (!hospitalId) {
            // temp1[0].length-1
            let hospitals = this.hospitalService.userHospitals.getValue()[0];
            this.helper.storageService.setSettings('HOSPITAL_ID', hospitals[hospitals.length - 1].hospitalId)
              .then(() => {
                this.helper.storageService.setLocalSettings('HOSPITAL_DATA', hospitals[hospitals.length - 1] );
              })
          }
        })
      this.hospitals = this.hospitalService.userHospitals.getValue()[0];
      this.events.publish('get:hospitals');
    })
  }

  sendOptions() {
    this.getHospitalLength();
    this.updateOptions.emit({
      hospitals: this.hospitalLength
    });
  }

  viewHospital(hospitalId: any, hospital: any) {
    let modal = this.modalCtrl.create(AddHospital, { hospitalId: hospitalId, hospitaldata: hospital });
    modal.present();
  }

  removeHospital(hospital: any) {
    this.helper.confirmPrompt('Delete Hospital', 'This process will delete all information associated with this hospital including patient information and cannot be recovered', 'Continue')
      .then(() => {
        this.helper.showLoading();
        if (hospital.isCustom == "true") {
          return this.hospitalService.updateUserHospitals({ hospitalId: hospital.hospitalId, isRemoved: "true" })
        } else {
          return this.hospitalService.removeUserHospital(hospital.hospitalId)
        }
      })
      .then(() => {
        if (!hospital.isCustom)
          return this.hospitalService.removeMailingInfo(hospital.hospitalId)
      })
      .then(() => {
        let index = _.findIndex(this.hospitals, hospital);

        if (index > -1) {
          this.hospitals.splice(index, 1);
          this.hospitalService.userHospitals.getValue().splice(index, 1);
        }
        this.sendOptions();
        this.helper.hideLoading();
        this.helper.showMessage('Hospital deleted successfully');
      })
      .catch(() => {
        this.helper.hideLoading();
        console.log('Error removing hospital');
      });
  }

}
