import { Component } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { CartList } from '../../pages/cart/cart-list/cart-list';
import { AnalyticsPage } from '../../pages/analytics/analytics';
import { Helper } from '../../providers/helper';

@Component({
  selector: 'footer-menu',
  templateUrl: 'footer-menu.html'
})

export class FooterMenu {

  tabs: any = [];
  badgeCount: number = 0;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public helper: Helper,
  ) {
    this.tabs = [{
      "outline": "ios-home-outline",
      "filled": "ios-home",
      "title": "Home",
      "value": HomePage
    }, {
      "outline": "ios-stats-outline",
      "filled": "ios-stats",
      "title": "Analytics",
      "value": AnalyticsPage
    },
    {
      "outline": "ios-filing-outline",
      "filled": "ios-filing",
      "title": "Billing",
      "value": CartList
    }];

    // listen to change in badge count, change accordingly
    this.events.subscribe('set-badge', (count) => {
      this.setBadge(count);
    });
    this.setBadge();
  }

  setBadge(count?) {
    if (!count) {
      this.helper.getTotalCartCount()
        .then((count: any) => {
          this.badgeCount = count ? +count : 0;
        });
    }
    else {
      this.badgeCount = count;
    }
  }

  viewtab(tab: any) {
    this.navCtrl.setRoot(tab);
  }

  isActive(tab: any) {
    return this.navCtrl.getActive().component.name == tab.name ? true : false;
  }
}
