import { Component, OnInit } from "@angular/core";
import { Events, Platform, MenuController, NavParams, NavController } from "ionic-angular";
import { TouchID } from '@ionic-native/touch-id';
import { CartList } from '../../pages/cart/cart-list/cart-list';
import { StatusBar } from '@ionic-native/status-bar';
import { Helper } from '../../providers/helper';
declare const cordova: any;

/* HTML Template */
const LOCK_SCREEN_TEMPLATE = `
      <div class="ILS_lock" [ngClass]="!_showLockScreen ?  'ILS_lock-hidden' : ''">
        <div class="ILS_label-row">
          {{passcodeLabel}}
        </div>
        <div class="ILS_icon-row" *ngIf="touchId">
          <img src="assets/img/finger-print.jpg">
        </div>
        <div class="ILS_circles-row" [ngClass]="passcodeWrong ?  'ILS_shake' : ''">
          <div class="ILS_circle" [ngClass]=" enteredPasscode.length>0 ? 'ILS_full' : ''"></div>
          <div class="ILS_circle" [ngClass]=" enteredPasscode.length>1 ? 'ILS_full' : ''"></div>
          <div class="ILS_circle" [ngClass]=" enteredPasscode.length>2 ? 'ILS_full' : ''"></div>
          <div class="ILS_circle" [ngClass]=" enteredPasscode.length>3 ? 'ILS_full' : ''"></div>
        </div>
        <div class="ILS_numbers-row">
          <div (tap)="digit(1)" class="ILS_digit">1</div>
          <div (tap)="digit(2)" class="ILS_digit">2</div>
          <div (tap)="digit(3)" class="ILS_digit">3</div>
        </div>
        <div class="ILS_numbers-row">
          <div (tap)="digit(4)" class="ILS_digit">4</div>
          <div (tap)="digit(5)" class="ILS_digit">5</div>
          <div (tap)="digit(6)" class="ILS_digit">6</div>
        </div>
        <div class="ILS_numbers-row">
          <div (tap)="digit(7)" class="ILS_digit">7</div>
          <div (tap)="digit(8)" class="ILS_digit">8</div>
          <div (tap)="digit(9)" class="ILS_digit">9</div>
        </div>
        <div class="ILS_numbers-row">
          <div *ngIf="ACDelbuttons" (tap)="allClear()" class="ILS_digit ILS_ac">AC</div>
          <div (tap)="digit(0)" class="ILS_digit">0</div>
          <div *ngIf="ACDelbuttons" (tap)="remove()" class="ILS_digit ILS_del">DEL</div>
        </div>
      </div>
    `;

/* Style */
const LOCK_SCREEN_STYLE = `
          /* Animations*/
          @keyframes ILS_shake {
            from, to {
              transform: translate3d(0, 0, 0);
            }
            10%, 30%, 50%, 70%, 90% {
              transform: translate3d(-10px, 0, 0);
            }
            20%, 40%, 60%, 80% {
              transform: translate3d(10px, 0, 0);
            }
          }
          @keyframes ILS_buttonPress {
            0% {
              background-color: #E0E0E0;
            }
            100% {
              background-color: #F1F1F1;
            }
          }
          /* Lock Screen Layout*/
          .ILS_lock {
            display: flex;
            flex-direction: column;
            justify-content: center;
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 999;
            background-color: #ffffff;
          }
          .ILS_lock-hidden {
            display: none;
          }
          .ILS_label-row {
            width: 100%;
            height: 50px;
            text-align: center;
            font-size: 23px;
            color: #464646;
          }
          .ILS_icon-row {
            width: 100%;
            text-align: center;
            margin-bottom:10px;
          }
          .ILS_circles-row {
            display: flex;
            flex-direction: row;
            justify-content: center;
            width: 100%;
          }
          .ILS_circle {
            background-color: #F1F1F1!important;
            border-radius: 50%;
            width: 10px;
            height: 10px;
            border:solid 1px #464646;
            margin: 0 15px;
          }
          .ILS_numbers-row {
            display: flex;
            flex-direction: row;
            justify-content: center;
            width: 100%;
            height: 100px;
          }
          .ILS_digit {
            margin: 0 14px;
            width: 80px;
            border-radius: 10%;
            height: 80px;
            text-align: center;
            padding-top: 29px;
            font-size: 21px;
            color: #464646;
            background-color: #ffffff;
          }
          .ILS_digit.activated {
            -webkit-animation-name: ILS_buttonPress;
            animation-name: ILS_buttonPress;
            -webkit-animation-duration: 0.3;
            animation-duration: 0.3s;
          }
          .ILS_ac {
            color: #464646;
            background-color: #ffffff;
            }
          .ILS_del {
            color: #464646;
            background-color: #ffffff;
            }
          .ILS_full {
            background-color:#464646!important;
          }
          .ILS_shake {
            -webkit-animation-name: ILS_shake;
            animation-name: ILS_shake;
            -webkit-animation-duration: 0.5;
            animation-duration: 0.5s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
          }
`;

@Component({
  selector: 'lock-screen',
  template: LOCK_SCREEN_TEMPLATE,
  styles: [LOCK_SCREEN_STYLE]
})
export class LockScreenComponent implements OnInit {

  _showLockScreen: boolean;
  ACDelbuttons: boolean;
  passcodeWrong: boolean;
  touchId: boolean;

  passcodeAttempts: number = 0;

  enteredPasscode: string = '';
  passcode: string;
  passcodeLabel: string;
  touchLabel: string;

  onCorrect: any;
  onWrong: any;
  selected: any;

  component: any;

  constructor(
    public events: Events,
    public platform: Platform,
    private navCtrl: NavController,
    private navParams: NavParams,
    public menuCtrl: MenuController,
    public helper: Helper,
    private statusBar: StatusBar,
    private iosTouchId: TouchID
  ) {
    this.menuCtrl.swipeEnable(false);
    this.statusBar.backgroundColorByHexString('#ffffff');
    this._showLockScreen = true;
    this.touchId = navParams.data.touchId || false;
    this.ACDelbuttons = navParams.data.ACDelbuttons || false;
    this.passcode = navParams.data.code;
    this.onCorrect = navParams.data.onCorrect || null;
    this.onWrong = navParams.data.onWrong || null;
    this.passcodeLabel = navParams.data.passcodeLabel || 'Enter Passcode';
    this.touchLabel = navParams.data.passcodeLabel || 'Verify Passcode';
    this.component = navParams.data.component;
  }

  ngOnInit() {
    setTimeout(() => {
      if ((this.platform.is('android') || this.platform.is('ios')) && this.touchId) {
        this.isFingerprintAvailable()
          .then((res) => {
            this.listenToFingerprintAuth(this.passcodeLabel);
          })
          .catch((err: any) => {
            console.log('err occured while instiating the fingerprint')
          });
      }
    }, 50);
  }

  allClear(): void {
    this.enteredPasscode = "";
  }


  isFingerprintAvailable() {
    return new Promise((resolve, reject) => {
      if (this.platform.is('android')) {
        cordova.plugins.Fingerprint.isAvailable(
          (res: any) => {
            if (res == 'AVAILABLE') {
              this.touchId = true;
              this.passcodeLabel = 'Touch ID or ' + this.passcodeLabel
              resolve();
            }
            else {
              console.log('An error regarding Fingerprint Sensor', res)
              reject('An error regarding Fingerprint Sensor');
            }
          },
          (err: any) => {
            reject(err);
          }
        )
      }
      else if (this.platform.is('ios')) {
        this.iosTouchId.isAvailable()
          .then(
          (res) => { resolve() },
          (err) => { reject('An Error Occured regarding Touch Id') }
          );
      }
    })
  }

  listenToFingerprintAuth(iOSPasscodeLabel?: any) {
    let self = this;
    if (this.platform.is('android')) {
      cordova.plugins.Fingerprint.listenForAuthentication({ clientId: 'fingerprint', clientSecret: 'fingerprint' },
        function(res: any) {
          if (res.code == 'SUCCESSFUL') {
            self.digit(self.passcode);
            if (self.component) {
              self.navigate(self.component);
            }
          }
          else {
            self.helper.showMessage(res.message, 3000);
            self.digit(9999);
          }
        }, function(err: any) { }
      )
    }
    else if (this.platform.is('ios')) {
      self.iosTouchId.verifyFingerprint(iOSPasscodeLabel).then(
        (res) => {
          self.digit(self.passcode);
          if (self.component) {
            self.navigate(this.component);
          }
        },
        (err) => {
          if (err == -1 || err == -8) {
            self.helper.showMessage("Unable to unlock the device with this fingerprint", 3000);
          }
        }
      )
    }
  }

  remove(): void {
    this.enteredPasscode = this.enteredPasscode.slice(0, -1);
  }

  digit(digit: any) {
    this.selected = +digit;
    if (this.passcodeWrong) {
      return false;
    }
    this.enteredPasscode += '' + digit;

    if (this.enteredPasscode.length >= 4) {
      if (this.enteredPasscode === '' + this.passcode) {
        this.enteredPasscode = '';
        this.passcodeAttempts = 0;
        this.onCorrect && this.onCorrect();
        this._showLockScreen = false;
        this.helper.showMessage('Authenticated successfully');
        if (this.component) {
          this.navigate(this.component);
        }
      } else {
        this.passcodeWrong = true;
        this.passcodeAttempts++;
        this.onWrong && this.onWrong(this.passcodeAttempts);
        setTimeout(() => {
          this.enteredPasscode = '';
          this.passcodeWrong = false;
        }, 800);
      }
    }
  }

  navigate(component) {
    if (localStorage.getItem('canGotoCart') && localStorage.getItem('canGotoCart') == '1') {
      localStorage.removeItem('canGotoCart');
      this.navCtrl.setRoot(CartList);
    }
    else {
      this.navCtrl.setRoot(component);
    }
  }

  ngOnDestroy() {
    this.statusBar.backgroundColorByHexString('#2C4099');
    this.menuCtrl.swipeEnable(true);
  }

}
