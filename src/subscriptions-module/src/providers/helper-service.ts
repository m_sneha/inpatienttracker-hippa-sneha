import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { LoadingController, ToastController } from 'ionic-angular';
import * as _ from 'underscore';
import { Auth } from 'aws-amplify';
import { EnvironmentsConfig } from '../environments/environments';

@Injectable()
export class SubscriptionHelperSerivce {
  loaderInstance: any;

  constructor(
    public environmentConfig: EnvironmentsConfig,
    public http: Http,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { }
  getsubscriptionPlans() {
    return new Promise((resolve, reject) => {

      let headers: any = this.getHeaders(this.environmentConfig.getApiKey(), 'stripe'),
        options = new RequestOptions({ headers: headers });
      this.http.get('https://api.stripe.com/v1/plans', options)
        .subscribe((data: any) => {
          console.log(data);
          resolve(data);
        },
          (err: any) => {
            reject(err);
          });
    })
  }

  getHeaders(token: any, src?: any) {
    let headers = new Headers();
    if (src == 'stripe') {
      headers.append('Authorization', `Bearer ${token}`);
    }
    else {
      headers.append('Authorization', token);
    }
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  createStripeCustomer(userData: any) {
    return new Promise(async (resolve, reject) => {
      let idToken = (await Auth.currentSession()).getIdToken().getJwtToken();
      console.log('idToken',idToken)
      let headers: any = this.getHeaders(idToken),
        options = new RequestOptions({ headers: headers }),
        body: any = JSON.stringify({ body: userData });
      this.http.post('https://g791tonko5.execute-api.us-east-1.amazonaws.com/Prod/createStripeCustomer', body, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  createStripeCharges(userData: any) {
    return new Promise(async(resolve, reject) => {
      let idToken = (await Auth.currentSession()).getIdToken().getJwtToken()
      let headers: any = this.getHeaders(idToken),        options = new RequestOptions({ headers: headers }),
        body: any = JSON.stringify({ body: userData });
      this.http.post(' https://8xgc9xghca.execute-api.us-east-1.amazonaws.com/Prod/stripeCreateCharges', body, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  getSubscribedPlans(stripeCustId: any) {
    return new Promise((resolve, reject) => {
      let headers: any = this.getHeaders(this.environmentConfig.getApiKey(), 'stripe'),
        options = new RequestOptions({ headers: headers });
      this.http.get(`https://api.stripe.com/v1/customers/${stripeCustId}/subscriptions`, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  getCustomerSources(stripeCustId: any) {
    return new Promise((resolve, reject) => {
      let headers: any = this.getHeaders(this.environmentConfig.getApiKey(), 'stripe'),
        options = new RequestOptions({ headers: headers });
      this.http.get(`https://api.stripe.com/v1/customers/${stripeCustId}/sources`, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  cancelSubsription(subscriptionId: any) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      let apiKey = this.environmentConfig.getApiKey();
      headers.append('Authorization', `Bearer ${apiKey}`);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      let params = new URLSearchParams();
      params.set('at_period_end', 'true');
      let options = new RequestOptions({ headers: headers, params: params.toString() });
      this.http.delete(`https://api.stripe.com/v1/subscriptions/${subscriptionId}`, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  detachCardSource(customerId: any, sourceId: any) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      let apiKey = this.environmentConfig.getApiKey();
      headers.append('Authorization', `Bearer ${apiKey}`);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      let options = new RequestOptions({ headers: headers });
      this.http.delete(`https://api.stripe.com/v1/customers/${customerId}/sources/${sourceId}`, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  attachSource(customerId: any, sourceId: any) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      let apiKey = this.environmentConfig.getApiKey();
      headers.append('Authorization', `Bearer ${apiKey}`);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      let params = new URLSearchParams();
      params.set('source', sourceId);
      let options = new RequestOptions({ headers: headers, params: params.toString() }),
        body: any = JSON.stringify({});
      this.http.post(`https://api.stripe.com/v1/customers/${customerId}/sources`, `source=${sourceId}`, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  changeDefaultSource(customerId: any, sourceId: any) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      let apiKey = this.environmentConfig.getApiKey();
      headers.append('Authorization', `Bearer ${apiKey}`);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      let params = new URLSearchParams();
      params.set('default_source', sourceId);
      let options = new RequestOptions({ headers: headers, params: params.toString() }),
        body: any = JSON.stringify({});
      this.http.post(`https://api.stripe.com/v1/customers/${customerId}`, `default_source=${sourceId}`, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }

  updateSubscription(subscriptionId: any, subscriptionPlanId: any) {
    return new Promise(async(resolve, reject) => {
      let idToken = (await Auth.currentSession()).getIdToken().getJwtToken()
      let headers: any = this.getHeaders(idToken),        options = new RequestOptions({ headers: headers }),
        body: any = JSON.stringify({ body: { subscriptionId: subscriptionId, subscriptionPlanId: subscriptionPlanId } });
      this.http.post('https://npk5vaukj6.execute-api.us-east-1.amazonaws.com/Prod/stripeUpdateSubscription', body, options)
        .subscribe((data: any) => {
          resolve(data);
        },
          (err: any) => {
            console.log('eror occured', err)
            reject(err);
          });
    })
  }


  // show Loading UI
  showLoading(title?) {
    this.loaderInstance = this.loadingCtrl.create({ "content": title || '' });
    this.loaderInstance.present();
  }

  // hide the Loading UI
  hideLoading() {
    if (this.loaderInstance) {
      this.loaderInstance.dismiss();
      this.loaderInstance = null;
    }
  }

  showMessage(msg: any, duration?: any, cssClass?: string) {
    this.toastCtrl.create({
      message: msg,
      duration: duration || 2000,
      cssClass: cssClass || ''
    }).present();
  }

  isSubscriptionActive(stripeCustId: any) {
    return new Promise((resolve, reject) => {
      this.getSubscribedPlans(stripeCustId)
        .then((response: any) => {
          if (JSON.parse(response._body) && JSON.parse(response._body).data) {
            let customerSubscriptions = JSON.parse(response._body).data;
            let currentSubscribedPlan = _.find(customerSubscriptions, (sub: any) => {
              return sub.status == 'trialing' || sub.status == 'active';
            });
            if (currentSubscribedPlan) {
              resolve({ isExists: true, data: currentSubscribedPlan })
            }
            else {
              resolve({ isExists: false, data: currentSubscribedPlan })
            }
          }
        })
        .catch((err: any) => {
          console.log('Err occured', err);
          resolve({ isExists: false, err: err, data: {} })
        })
    })
  }
}
